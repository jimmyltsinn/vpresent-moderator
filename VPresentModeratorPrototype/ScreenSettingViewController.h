//
//  FirstViewController.h
//  TabAppTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface ScreenSettingViewController : UITableViewController

@property (strong, nonatomic) NSArray* cellLabels;
@property (strong, nonatomic) NSArray* sectionLabels;

@end
