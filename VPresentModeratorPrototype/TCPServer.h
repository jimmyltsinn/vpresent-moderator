//
//  Server.h
//  ConnectionDemo
//
//  Created by Jimmy Sinn on 21/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkUtilities.h"

@interface TCPServer : NSObject
@property (nonatomic) NSMutableDictionary *clients;

- (void) registerLog: (id) object selector: (SEL) selector;

- (void) registerAcceptClientCallback: (id) object selector: (SEL) selector;

- (BOOL) listenTo: (unsigned short) listenPort;
- (void) registerReceiveCallback: (id) object selector: (SEL) selector;
- (void) closeAcceptSocket; 

- (void) sendDataToClient: (NSFileHandle *) fileHandle data: (NSData *) data;

@end


