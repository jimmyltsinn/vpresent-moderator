//
//  FirstViewController.m
//  TabAppTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "ScreenSettingViewController.h"
#import "DetailViewController.h"
#import "NetworkUtilities.h"

@interface ScreenSettingViewController ()

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@property (nonatomic) bool isConnectingExternal;

@end

@implementation ScreenSettingViewController

@synthesize cellLabels;
@synthesize sectionLabels;
@synthesize mainTableView;

@synthesize isConnectingExternal;


- (void) screenDidChange: (NSNotification *) notification {
    /* NSMutableArray* tempResolutionLabels = [[NSMutableArray alloc] init];
    
    NSLog(@"%d", [[UIScreen screens] count]);
    
    if ([[UIScreen screens] count] > 1) {
        UIScreen* extScreen = [[UIScreen screens] objectAtIndex: 1];
        NSArray* availableModes = [extScreen availableModes];
        
        for(UIScreenMode* mode in availableModes){
            uint32_t width = mode.size.width;
            uint32_t height = mode.size.height;
            
            NSString* tempStr = [NSString stringWithFormat:@"%d x %d", width, height];
            NSLog(@"%@", tempStr);
            [tempResolutionLabels addObject:tempStr];
        }
    }
    
    cellLabels = [tempResolutionLabels copy];
    */
    
    //
    if ([[UIScreen screens] count] > 1)
        isConnectingExternal = YES;
    else
        isConnectingExternal = NO;
    
    NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:0];
    [self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self.mainTableView reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated
{
//    NSLog(@"hi");
    self.tabBarController.title = self.tabBarItem.title;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self screenDidChange: Nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidChange:)
												 name:UIScreenDidConnectNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidChange:)
												 name:UIScreenDidDisconnectNotification
											   object:nil];
    // Section labels
    [self loadContent];
}

-(void)loadContent {
    self.sectionLabels = [[NSArray alloc] initWithObjects: @"IP Address", @"External Status", @"External Switch", nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch( [indexPath section] ) {
        case 0: {
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"IP Address"];
            if( aCell == nil ) {
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IP Address"];
            }
            aCell.textLabel.text = @"IP Address";
            aCell.selectionStyle = UITableViewCellSelectionStyleNone;
            aCell.userInteractionEnabled = NO;
            aCell.detailTextLabel.text = [NetworkUtilities deviceIPAddress];// @"hihi";//(isConnectingExternal ? @"Yes" : @"No");
            
            return aCell;
        }break;

            // Section 0: Number of Screen Connected
        case 1: {
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"External Cell"];
            if( aCell == nil ) {
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"External Cell"];
            }
            aCell.textLabel.text = @"Connected to Projector";
            aCell.selectionStyle = UITableViewCellSelectionStyleNone;
            aCell.userInteractionEnabled = NO;
            aCell.detailTextLabel.text = (isConnectingExternal ? @"Yes" : @"No");
            
            return aCell;
        }break;
            
        // Section 1: Master Switch to External Screen
        case 2: {
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Switch Cell"];
            if( aCell == nil ) {
                NSLog(@"a");
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Switch Cell"];
            }
            aCell.textLabel.text = @"Output to Projector";
            aCell.selectionStyle = UITableViewCellSelectionStyleNone;
            UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
            aCell.accessoryView = switchView;
            [switchView setOn:NO animated:NO];
            switchView.on = YES;
            [switchView addTarget:self action:@selector(externalSwitchChanged:) forControlEvents:UIControlEventValueChanged];

            return aCell;
        }break;
        
        /* Section 2: List of available resolution of external screen
        case 2:{
            //NSLog([cellLabels description]);
            if(cellLabels){
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Resolution Cell"];
                
                NSLog([cell description]);
                
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Resolution Cell"];
                }
                
                // Config the cell..
                cell.textLabel.text = [cellLabels objectAtIndex:[indexPath item]];
                NSLog(@"Inside cellForRowAtIndexPath: %@", cell.textLabel.text);
                return cell;
            }
        }break;*/
    }
    return nil;
}

// For controlling the external output switch
- (void) externalSwitchChanged:(id)sender {
    UISwitch* switchControl = sender;
    
    // Get the current View Controller at the right (DetailViewController)
    //UINavigationController* masterNavigationViewController = [self.splitViewController.viewControllers objectAtIndex:1];
    //DetailViewController* detailViewController = (DetailViewController*)[masterNavigationViewController visibleViewController];
    
    DetailViewController* detailViewController = [self.splitViewController.viewControllers objectAtIndex:1];
    
    if (switchControl.on) {
        if (detailViewController.numberOfScreen > 1) {
            [detailViewController.slides enableExternalView: [[UIScreen screens] objectAtIndex: 1]];
        } else {
            [detailViewController showErrorPrompt:@"No external monitor" description:@"Cannot enable external monitor. You did not connect to external monitor"];
            switchControl.on = NO;
        }
    } else {
        [detailViewController.slides disableExternalView];
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [sectionLabels objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Do some stuff when the row is selected
    //NSLog(@"Hidden!LOL");
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setMainTableView:nil];
    [super viewDidUnload];
}
@end
