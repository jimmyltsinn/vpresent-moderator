//
//  PendingRequestsViewController.m
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 20/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPModeratorPendingRequestsViewController.h"
#import "VPModeratorSplitViewController.h"
#import "VPModeratorPresenter.h"

@interface VPModeratorPendingRequestsViewController ()

@property (strong, nonatomic) VPModeratorSplitViewController* splitVC;
@property (strong, nonatomic) IBOutlet UITableView *mainTableView;

@end

@implementation VPModeratorPendingRequestsViewController

//@synthesize cellLabels;
@synthesize notificationCount = _notificationCount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSNumber *)notificationCount {
    return _notificationCount;
}

- (void)setNotificationCount:(NSNumber *)notificationCount {
    _notificationCount = notificationCount;
    self.tabBarItem.badgeValue = [_notificationCount stringValue];
    
    return;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.title = self.tabBarItem.title;
    self.notificationCount = [NSNumber numberWithInt:0];
    self.tabBarItem.badgeValue = nil;
    
    [self reloadRequestListView];
}

- (void)reloadRequestListView {
    NSIndexSet* tempSection = [[NSIndexSet alloc]initWithIndex:0];
    [self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.splitVC = (VPModeratorSplitViewController*)self.splitViewController;
    
    // Init. of cell lables
    //cellLabels = [[NSArray alloc]initWithObjects:@"640 X 480", @"800 X 600", @"1024 X 768", nil];
    //self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", cellLabels.count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [((VPModeratorSplitViewController *)self.splitViewController).requestList count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([indexPath section]) {
        // Section 0: Request Cells
        case 0: {
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"Request Cell"];
            if( aCell == nil ) {
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Request Cell"];
            }
            aCell.textLabel.text = ((VPModeratorPresenter *)[((VPModeratorSplitViewController *)self.splitViewController).requestList objectAtIndex:[indexPath row]]).presenterName;
            aCell.detailTextLabel.text = @"Request Control"; //Hard code...lol
            aCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            
            return aCell;
        }break;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath section]) {
        // Section 0: Request Cells
        case 0: {
            [self.splitVC.requestList removeObjectAtIndex:[indexPath row]];
            NSLog(@"%@", [self.splitVC.requestList description]);
            NSLog(@"%d", self.splitVC.requestList.count);
        }break;
    }
    [tableView reloadSections:[[NSIndexSet alloc]initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)viewDidUnload {
    [self setMainTableView:nil];
    [super viewDidUnload];
}
@end
