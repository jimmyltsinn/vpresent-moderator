//
//  Timer.h
//  VPPresenterPrototype
//
//  Created by Jimmy Sinn on 26/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Timer : NSObject

- (void)start;
- (void)pause;
- (void)end;
- (int)currentTime;
- (NSString *)currentTimeInString; 
- (void)setEachSecondAction: (id)target selector:(SEL)selector;
- (void)resetEachSecondAction; 
@end
