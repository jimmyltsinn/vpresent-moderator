//
//  SecondViewController.h
//  TabAppTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPModeratorPresenter.h"
#import "DetailViewController.h"
#import "VPModeratorSplitViewController.h"

@interface PresenterListViewController : UITableViewController

//@property (strong, nonatomic) NSArray* presenterList;

//@property (strong, nonatomic) VPModeratorPresenter* currentPresenter;

@end
