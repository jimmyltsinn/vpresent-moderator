//
//  Server.m
//  ConnectionDemo
//
//  Created by Jimmy Sinn on 21/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//
//  Reference: http://changyy.pixnet.net/blog/post/29451765-iphone-%E9%96%8B%E7%99%BC%E6%95%99%E5%AD%B8---a-simple-socket-server-example

#import "TCPServer.h"

#import <CFNetwork/CFNetwork.h>
#import <arpa/inet.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netdb.h>

@interface TCPServer()

@property (nonatomic) CFSocketRef socket;
@property (nonatomic) NSFileHandle *fileHandle;

@property (nonatomic) id acceptClientCallbackObject;
@property (nonatomic) SEL acceptClientCallbackSelector;

@property (nonatomic) id receiveCallbackObject;
@property (nonatomic) SEL receiveCallbackSelector;

@property (nonatomic) id loggingObject;
@property (nonatomic) SEL loggingSelector;

@property (nonatomic) unsigned short listenPort;

@property (nonatomic) int state;

@end

@implementation TCPServer

@synthesize socket;
@synthesize fileHandle;
@synthesize listenPort;
@synthesize state;
@synthesize clients = _clients;

@synthesize acceptClientCallbackObject;
@synthesize acceptClientCallbackSelector;
@synthesize receiveCallbackObject;
@synthesize receiveCallbackSelector;
@synthesize loggingObject;
@synthesize loggingSelector;

- (NSMutableDictionary *) clients {
    if (!_clients) {
        _clients = [[NSMutableDictionary alloc] init];
    }
    return _clients;
}

/* Debug purpose */
- (void) registerLog: (id) object selector: (SEL) selector {
    if (![object respondsToSelector:selector]) {
        [NSException raise: @"Invalid selector for Log" format: @"Object of %@ do not respond to selector %@", [[object class] description], NSStringFromSelector(selector)];
        self.loggingObject = nil;
        self.loggingSelector = nil;
    } else {
        self.loggingObject = object;
        self.loggingSelector = selector;
    }
    return;
}

- (void) logMessage: (NSString *) msg {
    [self.loggingObject performSelector: loggingSelector withObject: msg];
    return;
}

/* Listen */
- (BOOL) listen {
    int ret;
    
    self.socket = CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP, 0, NULL, NULL);
    
    if (!self.socket) {
        [self logMessage: [NSString stringWithFormat:@"Socket cannot create (%d): %s", errno, strerror(errno)]];
        return NO;
    }
    
    long val;
    ret = setsockopt(CFSocketGetNative(self.socket), SOL_SOCKET, SO_REUSEADDR, &val, sizeof(long));
    if (ret < 0) {
        [self logMessage: [NSString stringWithFormat:@"Cannot set socket reuse (%d): %s", errno, strerror(errno)]];
    }
    
	struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
	addr.sin_len = sizeof(struct sockaddr_in);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(self.listenPort);
	addr.sin_addr.s_addr = INADDR_ANY;
    
    CFDataRef addrcfd = CFDataCreate(kCFAllocatorDefault, (UInt8 *) &addr, sizeof(addr));
    
    ret = CFSocketSetAddress(self.socket, addrcfd);
    if (ret != kCFSocketSuccess) {
        [self logMessage: [NSString stringWithFormat: @"Cannot bind socket address (%d)", ret]];
        CFRelease(self.socket);
        return NO;
    }
    
    self.fileHandle = [[NSFileHandle alloc] initWithFileDescriptor: CFSocketGetNative(self.socket) closeOnDealloc: YES];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(accept:) name: NSFileHandleConnectionAcceptedNotification object: nil];
    [self.fileHandle acceptConnectionInBackgroundAndNotify];
    
    [self logMessage: [NSString stringWithFormat: @"Start Listening on Port = %d", self.listenPort]];
    return YES;
}

- (BOOL) listenTo: (unsigned short) inListenPort {
    self.listenPort = inListenPort;
    return [self listen];
}

/* Accept */
- (void) registerAcceptClientCallback: (id) object selector: (SEL) selector {
    if (![object respondsToSelector:selector]) {
        [NSException raise: @"Invalid selector for receive data" format: @"Object of %@ do not respond to selector %@", [[object class] description], NSStringFromSelector(selector)];
        self.acceptClientCallbackObject = nil;
        self.acceptClientCallbackSelector = nil;
    } else {
        self.acceptClientCallbackObject = object;
        self.acceptClientCallbackSelector = selector;
    }
    return;
}

- (void) accept: (NSNotification *) notification {
    NSFileHandle *incomingFileHandle = [[notification userInfo] objectForKey: NSFileHandleNotificationFileHandleItem] ;
    
    if (incomingFileHandle) {
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(receive:) name: NSFileHandleDataAvailableNotification object: incomingFileHandle];
        [incomingFileHandle waitForDataInBackgroundAndNotify];
    }
    
    [self.fileHandle acceptConnectionInBackgroundAndNotify];
    
    struct sockaddr_in peer;
    unsigned int len = sizeof(peer);
    getpeername(incomingFileHandle.fileDescriptor, (struct sockaddr *) &peer, (socklen_t *) &len);
    
    [self logMessage: [NSString stringWithFormat: @"Accept Client: %@", [NetworkUtilities getAddressString:peer]]];
    
    [self.clients setObject: incomingFileHandle forKey: [NetworkUtilities getAddressString:peer]];
    
    [self.acceptClientCallbackObject performSelector: self.acceptClientCallbackSelector];
    
    return;
}

/* Receive data */
- (void) registerReceiveCallback: (id) object selector: (SEL) selector {
    if (![object respondsToSelector:selector]) {
//        NSLog(@"HIHI");
        [NSException raise: @"Invalid selector for receive data" format: @"Object of %@ do not respond to selector %@", [[object class] description], NSStringFromSelector(selector)];
        self.receiveCallbackObject = nil;
        self.receiveCallbackSelector = nil;
    } else {
        self.receiveCallbackObject = object;
        self.receiveCallbackSelector = selector;
    }
    return;
}

- (void) receive: (NSNotification *) notification {
    NSFileHandle *incomingFileHandle = [notification object];
    NSData *data = [incomingFileHandle availableData];
    
    [self logMessage: [NSString stringWithFormat: @"Receive Data from Client (len = %d)", [data length]]];
    
    if ([data length] == 0) {
        [self closeClient: incomingFileHandle];
        return;
    }
    
    [incomingFileHandle waitForDataInBackgroundAndNotify];
    
    [self.receiveCallbackObject performSelector: self.receiveCallbackSelector withObject: incomingFileHandle withObject: data];
    
    return;
}

/* Send */
- (void) sendDataToClient: (NSFileHandle *)incomingFileHandle data: (NSData *) data{
    [incomingFileHandle writeData:data];
    return;
}

/* Close */
- (void) closeClient: (NSFileHandle *) incomingFileHandle {
    struct sockaddr_in peer;
    unsigned int len = sizeof(peer);
    getpeername(incomingFileHandle.fileDescriptor, (struct sockaddr *) &peer, (socklen_t *) &len);
    
    [self logMessage:@"Close connection"];
    [incomingFileHandle closeFile];
    
    [self.clients removeObjectForKey: [NetworkUtilities getAddressString:peer]];
    [self.acceptClientCallbackObject performSelector: self.acceptClientCallbackSelector];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name: NSFileHandleDataAvailableNotification object: self.fileHandle];
    return;
}

- (void) closeAcceptSocket {
    [self logMessage:@"End of accepting"];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name: NSFileHandleConnectionAcceptedNotification object: nil];
    CFSocketInvalidate(self.socket);
    CFRelease(self.socket);
    
    self.listenPort = -1;
    
    [self.fileHandle closeFile];
    
    return;
}



@end
