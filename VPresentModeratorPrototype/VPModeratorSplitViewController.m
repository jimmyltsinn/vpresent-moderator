//
//  VPModeratorSplitViewController.m
//  VPServerPrototype
//
//  Created by Jimmy Sinn on 28/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPModeratorSplitViewController.h"
#import "VPModeratorDetailViewController.h"
#import "VPModeratorPendingRequestsViewController.h"
#import "VPModeratorMasterTabBarViewController.h"
#import "VPModeratorPresenterListViewController.h"
#import "VPModeratorServer.h"
#import "VPCanvas.h"

@interface VPModeratorSplitViewController ()
@property (nonatomic, strong) VPModeratorServer *server;

@property (strong, nonatomic) MPMoviePlayerController* player;

// Other VC reference
@property (nonatomic, strong) VPModeratorDetailViewController *detailVC;
@property (nonatomic, strong) VPModeratorPendingRequestsViewController *pendingVC;
@property (nonatomic, strong) VPModeratorMasterTabBarViewController *tabBarVC;
@property (nonatomic, strong) VPModeratorPresenterListViewController *listVC;

@end

@implementation VPModeratorSplitViewController

@synthesize presenterList = _presenterList;
@synthesize server;
@synthesize player;
@synthesize currentPresenter;
@synthesize requestList = _requestList;
@synthesize currentImage;

@synthesize detailVC;
@synthesize pendingVC;
@synthesize tabBarVC;
@synthesize listVC;

- (NSMutableArray *)requestList {
    if (_requestList == nil) {
        _requestList = [[NSMutableArray alloc] init];
    }
    return _requestList;
}

- (NSMutableDictionary *)presenterList {
    if (_presenterList == nil)
        _presenterList = [[NSMutableDictionary alloc] init];
    return _presenterList;
}

- (void) showErrorPrompt: (NSString *) title description: (NSString *) description {
    [[[UIAlertView alloc] initWithTitle:title message:description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        // Custom initialization
    }
    return self;
}

- (void)startServer {
//    NSLog(@"start server");
    [self.server listenTo:44300];
    return; 
}

- (void)stopServer {
    [self.server closeAcceptSocket];
    self.server = nil; 
}

- (void)logMessage:(NSString *)msg {
//    NSLog(@"%@",msg);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    NSLog(@"@@");
    self.server = [[VPModeratorServer alloc] initWithController:self];
    self.currentPresenter = nil;
    [self.server registerLog:self selector:@selector(logMessage:)];
//    if ([[self.viewControllers objectAtIndex:0].masterViewController isKindOfClass:[LeftTabBarViewController class]] == NO) {
        
//        [NSException raise:@"hi" format:@""];
//    }

//    ((PendingRequestsViewController *)((LeftTabBarViewController *)[self.viewControllers objectAtIndex:0]).viewControllers[2]).notificationCount = [NSNumber numberWithInt:[((PendingRequestsViewController *)((LeftTabBarViewController *)[self.viewControllers objectAtIndex:0]).viewControllers[2]).notificationCount integerValue] + 1];
    
    // Other VC reference
    detailVC = [self.viewControllers objectAtIndex:1];
    
    UINavigationController* naviVC = [self.viewControllers objectAtIndex:0];
    tabBarVC = [naviVC.viewControllers objectAtIndex:0];
    
    listVC = [tabBarVC.viewControllers objectAtIndex:1];
    pendingVC = [tabBarVC.viewControllers objectAtIndex:2];
    
    [self startServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
//    NSLog(@"fuuuuuu");
}

- (BOOL)registerRequest {
    // Update presenter list
    NSLog(@"Register Request");
    [self.listVC reloadPresenterList];
    [self showErrorPrompt:@"Message" description:@"Presenter Connected"];
    
    // actually, nothing to do here.....
    // all registration done in VPModeratorServer
    
    return YES; 
}

- (BOOL)handleUnregister: (char)presenterID {
    if (self.currentPresenter.presenterID == presenterID) {
        //suppose
        //NSLog(@"Current presenter trying to unregister");
        //[self showErrorPrompt:@"Message" description:@"Current Presenter try to unregister"];
        return NO;
    }
    
    NSLog(@"Unregister Request");
    
    [self.presenterList removeObjectForKey:[NSNumber numberWithChar:presenterID]];
    [self.listVC reloadPresenterList];
    [self showErrorPrompt:@"Message" description:@"Presenter Unregister"];
    return YES;
}

- (BOOL)handleControlRequest: (char)presenterID {
    NSLog(@"Control request");
    [self.requestList addObject:[self.presenterList objectForKey:[NSNumber numberWithChar:presenterID]]];
    
    // Increment of notification count by 1 ... LOL
    ((VPModeratorPendingRequestsViewController *)([((VPModeratorMasterTabBarViewController *)(((UINavigationController *)[self.viewControllers objectAtIndex:0]).topViewController)).viewControllers objectAtIndex:2])).notificationCount = [NSNumber numberWithInt:[((VPModeratorPendingRequestsViewController *)([((VPModeratorMasterTabBarViewController *)(((UINavigationController *)[self.viewControllers objectAtIndex:0]).topViewController)).viewControllers objectAtIndex:2])).notificationCount integerValue] + 1];
    
    // Reload Request List View
    [self.pendingVC reloadRequestListView];
    
    return YES;
}

- (short)handleSlide: (char)presenter slide: (UIImage *)image {
    // From original detail view
    // Remove subview (slide and canvas) of previous slide
    NSLog(@"handleSlide:slide:");
    if (self.currentPresenter.presenterID == presenter) {
        [detailVC.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 123];
        [detailVC.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 124];
        
        // Create subview for first image
        CGRect frame = {{0, 0}, {detailVC.mainSlidesContainer.frame.size.width, detailVC.mainSlidesContainer.frame.size.height}};
        
        //NSLog(@"%@", [imageList objectAtIndex:0]);
        UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
        imageView.frame = frame;
        imageView.clipsToBounds = YES;
        [detailVC.slides addSubview:imageView withKey: @"Test image" withTag:123];
        
        VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
        [detailVC.slides addSubview:canvas withKey:@"canvas" withTag:124];
    } else {
        [self logMessage:[NSString stringWithFormat:@"Not id=%x turn", presenter]];
    }
    
    
    return 0;
}

- (char)handleControlToSlide:(char)presenterID slide:(int)imageIndex {
    [self.detailVC.slides performOnPDF:^(PSPDFViewController *pdfController) {
        [pdfController setPage:imageIndex animated:YES];
    }];
    return 0;
}

- (void)handlePDFDocument:(char)presenterID document:(PSPDFDocument *)document {
    if(self.currentPresenter.presenterID == presenterID){
        [self.detailVC.slides performOnPDF:^(PSPDFViewController *pdfController) {
            pdfController.document = document;
            [pdfController reloadData];
        }];
    }
    return;
}

- (void)handlePDFAnnotation:(char)presenterID annotation:(PSPDFAnnotation *)annotation onPage:(int)page {
    if(self.currentPresenter.presenterID == presenterID){
        [self.detailVC.slides performOnPDF:^(PSPDFViewController *pdfController) {
            NSLog(@"addAnnotation: %@", [annotation description]);
            [pdfController.document addAnnotations:[NSArray arrayWithObject:annotation] forPage:page];
            [pdfController reloadData];
        }];
    }
    return;
}

- (void)handlePDFViewState:(char)presenterID viewState:(PSPDFViewState *)viewState {
    if(self.currentPresenter.presenterID == presenterID){
        CGPoint contentOffset = viewState.contentOffset;
        [self.detailVC.slides performOnPDF:^(PSPDFViewController *pdfController, CGPoint point) {
            viewState.contentOffset = point;
            [pdfController setViewState:viewState animated:YES];
            //        pdfController.viewState = viewState;
        } withPoint:contentOffset];
    }
    
}

- (char)handleControlClearDrawing:(char)presenterID{
    NSLog(@"handleControlClearDrawing:");
    
    if(presenterID == currentPresenter.presenterID){
        VPCanvas* canvas = (VPCanvas*)[self.detailVC.slides.deviceView viewWithTag:124];
        [[self.detailVC.slides.deviceView viewWithTag:124] removeFromSuperview];
        
        // Create subview for new image
        CGRect frame = {{0, 0}, {self.detailVC.mainSlidesContainer.frame.size.width, self.detailVC.mainSlidesContainer.frame.size.height}};
        
        // Recreate a new canvas
        canvas = [[VPCanvas alloc] initWithFrame:frame];
        canvas.tag = 124;
        [self.detailVC.slides.deviceView addSubview:canvas];
        
        // Set back the color and thickness
        //canvas.color = currentColor;
        //canvas.thickness = currentThickness;
    } else {
        // try to clear other presenter's screen....lol......
        
        // Error!!
        return 1;
    }
    
    // Assume no error
    return 0;
}

- (char)handleControlSignal: (char)presenter signal: (char)signal {
    
    return YES;
}

- (BOOL)handlePathDrawing: (char)presenter point: (CGPoint)point status:(char)status{
//    VPModeratorDetailViewController *detailViewController = [self.viewControllers objectAtIndex:1];
//    NSLog(@"POINT = %@", NSStringFromCGPoint(point));
    
//    NSLog(@"hi");
    
    if (self.currentPresenter.presenterID == presenter){
        if (status == 1) {
            //        NSLog(@"Begin = %@", NSStringFromCGPoint(point));
            [detailVC.slides perform: ^(UIView * inputView, CGPoint point) {
                [((VPCanvas *) inputView) penDown:point];
            } onKey: @"canvas" withPoint: point];
        } else if (status == 2) {
            //        NSLog(@"Up = %@", NSStringFromCGPoint(point));
            
            [detailVC.slides perform: ^(UIView * inputView, CGPoint point) {
                [((VPCanvas *) inputView) penUp:point];
            } onKey: @"canvas" withPoint: point];
        } else {
            //        NSLog(@"Moved = %@", NSStringFromCGPoint(point));
            
            [detailVC.slides perform: ^(UIView * inputView, CGPoint point) {
                [((VPCanvas *) inputView) penMove:point];
            } onKey: @"canvas" withPoint: point];
        }
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)handlePathDrawingSetting: (UIColor *)color size:(short)size {
    [self.detailVC.slides perform: ^(UIView *inputView) {
        ((VPCanvas *)inputView).color = color;
        ((VPCanvas *)inputView).thickness = size;
    } onKey: @"canvas"];
    return YES; 
}

- (BOOL) handlePlay:(char)presenterID filename:(NSString *)videoFilename {
    NSLog(@"Remote Play, filename = %@", videoFilename);
    if(self.currentPresenter.presenterID == presenterID){
        [self.player play];
    }
    return YES;
}

- (BOOL) handlePause:(char)presenterID {
    NSLog(@"Remote Pause");
    if(self.currentPresenter.presenterID == presenterID){
        [self.player pause];
    }
    return YES;
}

- (BOOL) handleStop:(char)presenterID {
    NSLog(@"Remote Stop");
    if(self.currentPresenter.presenterID == presenterID){
        [self.player stop];
        [self.detailVC.slides switchToPDF];
    }
    return YES;
}

- (BOOL) handleGoto:(char)presenterID goto:(double)second {
    NSLog(@"Remote Goto %f", second);
    if(self.currentPresenter.presenterID == presenterID){
        [self.player play];
        self.player.currentPlaybackTime = second;
        [self.player pause];
    }
    return YES;
}

- (short) handleVideo:(char)presenterID video:(NSData *)videoData {
    NSLog(@"handleVideo: video");
    
    if(self.currentPresenter.presenterID == presenterID){
        NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        NSString *cacheFile = [cachesPath stringByAppendingPathComponent:@"Movie.mp4"];
        NSLog(@"Cache File: %@", cacheFile);
        
        //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //    NSString *documentsDirectory = [paths objectAtIndex:0];
        //    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"myMove.mp4"];
        
        [videoData writeToFile:cacheFile atomically:YES];
        NSURL *moveUrl = [NSURL fileURLWithPath:cacheFile];
        NSLog(@"%@", [moveUrl description]);
        player = [[MPMoviePlayerController alloc]init];
        [player setContentURL:moveUrl];
        NSLog(@"%@", [player description]);
        //player.view.frame = viewPlayer.bounds;
        //[viewPlayer addSubview:player.view];
        //[player play];
        
        
        [player prepareToPlay];
        
        
        //設定影片比例的縮放、重複、控制列等參數
        player.shouldAutoplay = NO;
        player.scalingMode = MPMovieScalingModeAspectFit;
        player.repeatMode = MPMovieRepeatModeNone;
        player.controlStyle = MPMovieControlStyleDefault;
        
        //自動縮放符合畫面比例
        
        player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        
        //[player.view setFrame: self.playerSubview.bounds];  // player's frame must match parent's
        //[self.playerSubview addSubview: player.view];
        
        [player.view setFrame: self.detailVC.mainSlidesContainer.bounds];
        //[self.detailVC.mainSlidesContainer addSubview:player.view];
        //[self.detailVC.slides.externalView addSubview:player.view];
        [self.detailVC.slides addVideoViewController:player];
        
        // ...
        //[player play];
    }
    
    return 0;
}

@end