//
//  SlidesView.m
//  ViewDemo
//
//  Created by Jimmy Sinn on 16/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "SlidesView.h"

@implementation SlidesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
