//
//  SecondViewController.m
//  TabAppTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPModeratorPresenterListViewController.h"

@interface VPModeratorPresenterListViewController ()

@property (strong, nonatomic) NSArray* sectionLabels;

@property (strong, nonatomic) VPModeratorSplitViewController* splitVC;
@property (strong, nonatomic) NSArray* presenterArray;

@end

@implementation VPModeratorPresenterListViewController

//@synthesize presenterList;
@synthesize sectionLabels;
//@synthesize currentPresenter;
@synthesize mainTableView;

@synthesize splitVC;
@synthesize presenterArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self loadContent];
}

- (void)reloadPresenterList{
    splitVC = (VPModeratorSplitViewController*)self.splitViewController;
    presenterArray = [self.splitVC.presenterList allValues];
    
    NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:0];
    [self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.title = self.tabBarItem.title;

//    splitVC = (VPModeratorSplitViewController*)self.splitViewController;
//    presenterArray = [self.splitVC.presenterList allValues];
//    
//    NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:0];
//    [self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self reloadPresenterList];
    
    [self loadContent];
}

-(void)loadContent {
    //VPModeratorPresenter* tempA = [[VPModeratorPresenter alloc]init];
    //[tempA.timer setEachSecondAction:self selector:@selector(updateTimer)];
    //VPModeratorPresenter* tempB = [[VPModeratorPresenter alloc]init];
    //[tempB.timer setEachSecondAction:self selector:@selector(updateTimer)];
    
    // Init. of cell labels
    //presenterList = [[NSArray alloc]initWithObjects:tempA, tempB, nil];
    // Init. of section labels
    if(sectionLabels == nil){
        sectionLabels = [[NSArray alloc] initWithObjects: @"Presenter List", @"", @"", nil];
    }
    // Set nil to currentPresenter
    //self.currentPresenter = nil;

}


- (void)updateTimer{
    //NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:0];
    //[self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    VPModeratorDetailViewController* detailViewController = [self.splitViewController.viewControllers objectAtIndex:1];
    detailViewController.presenterLabel.text = splitVC.currentPresenter.presenterName;
    detailViewController.timerLabel.text = [splitVC.currentPresenter.timer currentTimeInString];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setMainTableView:nil];
    [self setMainTableView:nil];
    [super viewDidUnload];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return splitVC.presenterList.count;
            break;
        case 1:
            return 1;
        break;
            
        case 2:
            return 1;
        break;
    }
    
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([indexPath section]) {
        // Section 0: Actual Presenter List
        case 0:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Presenter Cell"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Presenter Cell"];
            }
            
            // Config the cell...
            VPModeratorPresenter* tempPresenter = [self.presenterArray objectAtIndex:[indexPath row]];
            cell.textLabel.text = tempPresenter.presenterName;
            if(tempPresenter == splitVC.currentPresenter)
                cell.detailTextLabel.text = @"Presenting";
            else
                cell.detailTextLabel.text = [tempPresenter.timer currentTimeInString];
            return cell;
        }
        break;
            
        // Section 1: Cut all Presenter Control
        case 1:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Reset Cell"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Reset Cell"];
            }
            
            // Config the cell...
            cell.textLabel.text = @"Stop Presenter Control";
            return cell;
        }
        break;
            
        // Section 2: Refersh Table View
        case 2:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Refresh Cell"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Refresh Cell"];
            }
            
            // Config the cell...
            cell.textLabel.text = @"Refresh List";
            return cell;
        }
        break;
    }
    
    return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [sectionLabels objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Do some stuff when the row is selected
    
    switch ([indexPath section]) {
        // Section 0: Actual Presenter List
        case 0:
        {
            // Get the VPPresenter object
            VPModeratorPresenter* selectedPresenter = [presenterArray objectAtIndex:[indexPath row]];
            VPModeratorDetailViewController* detailViewController = [self.splitViewController.viewControllers objectAtIndex:1];
            
            // if currentPresenter == selectedPresenter, do nothing
            if(!(splitVC.currentPresenter == selectedPresenter)){
                // if currentPresenter != nil, switch turn
                if(splitVC.currentPresenter != nil){
                    // Current Presenter
                    [splitVC.currentPresenter.timer resetEachSecondAction];
                    [splitVC.currentPresenter.timer pause];
                    
                    // Cut current presenter control
                    [self.splitVC.currentPresenter withdrawControl];
                    
                    // Switch Turn
                    splitVC.currentPresenter = selectedPresenter;
                
                    // Next Presenter
                    [splitVC.currentPresenter.timer setEachSecondAction:detailViewController selector:@selector(updateTimer)];
                    [selectedPresenter.timer start];
                    [splitVC.currentPresenter grantControl];
                } else {
                    splitVC.currentPresenter = selectedPresenter;
                    [splitVC.currentPresenter.timer setEachSecondAction:detailViewController selector:@selector(updateTimer)];
                    [splitVC.currentPresenter.timer start];
                    [splitVC.currentPresenter grantControl];
                }
            }
            
            // Update detailViewController
            detailViewController.presenterLabel.text = selectedPresenter.presenterName;
            //detailViewController.timerLabel.text = @"00:00:00";
        }
            break;
            
        // Section 1: Cut all Presenter Control
        case 1:
        {
            [self.splitVC.currentPresenter.timer resetEachSecondAction];
            [self.splitVC.currentPresenter.timer pause];
            // Cut current presenter control...
            [self.splitVC.currentPresenter withdrawControl];
            
            // Set current presenter to nil
            splitVC.currentPresenter = nil;
            
            // Update detailViewController
            VPModeratorDetailViewController* detailViewController = [self.splitViewController.viewControllers objectAtIndex:1];
            detailViewController.presenterLabel.text = @"";
            detailViewController.timerLabel.text = @"00:00:00";
        }
        break;
        
        // Section 2: Refresh List
        case 2:
        {
            splitVC = (VPModeratorSplitViewController*)self.splitViewController;
            presenterArray = [self.splitVC.presenterList allValues];
        }
        break;
    }
    
    NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:0];
    [self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
