//
//  SlidesViewController.h
//  ViewDemo
//
//  Created by Jimmy Sinn on 16/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSPDFKit/PSPDFKit.h>

#import "VPCanvas.h"

@interface VPSlidesViewController : UIViewController<PSPDFViewControllerDelegate, PSPDFAnnotationToolbarDelegate, PSPDFDocumentDelegate>

@property (strong, nonatomic) UIColor *backgroundColor;
@property (strong, nonatomic) UIView *deviceView;
@property (strong, nonatomic) UIView *externalView;
@property (strong, nonatomic) PSPDFDocument *showingPDF;
@property (strong, nonatomic) PSPDFViewController *externalPdfViewController;

- (UIView *) enableExternalView: (UIScreen *) externalScreen;
- (void) disableExternalView;
- (void)switchToPDF; 

//- (void) addSubview: (UIView *) view;
//- (void) addSubview: (UIView *) inputView withKey: (NSString *) key;

// TODO Manage tag to avoid duplication
- (void) addSubview:(UIView *)inputView withKey:(NSString *)key withTag: (NSInteger) tag;

/* Perform action on both views
 * Please input block of code
 */
- (void) perform: (void (^) (UIView *)) action onKey: (NSString *) key;
- (void) perform: (void (^) (UIView *)) action onTag: (NSInteger) tag;
- (void) perform: (void (^) (UIView *, CGPoint)) action onTag: (NSInteger) tag withPoint: (CGPoint) point;
- (void) perform: (void (^) (UIView *, CGPoint)) action onKey: (NSString *) key withPoint: (CGPoint) point;

- (void) performOnPDF: (void (^) (PSPDFViewController *)) action;
- (void) performOnPDF: (void (^)(PSPDFViewController *, CGPoint))action withPoint:(CGPoint)point;
//- (void) performOnPDF: (void (^)(PSPDFViewController *, CGRect))action withRect:(CGRect)rect;

- (void) setSubviewsFrame: (CGRect) frame;

- (void) addSubViewController: (UIViewController *)inputViewController;
- (void) addVideoViewController: (MPMoviePlayerController *)inputVideoViewController;
//- (void) changePDFDocument:(PSPDFDocument*)doc; 
//- (void) demoChangeText: (NSString *) str;
@end
