//
//  SlidesViewController.m
//  ViewDemo
//
//  Created by Jimmy Sinn on 16/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "SlidesViewController.h"

@interface SlidesViewController()

@property (strong, nonatomic, readonly) NSMutableSet *subviews;

@property (strong, nonatomic) UIWindow *externalWindow;
@property (nonatomic, strong) IBOutlet UISwipeGestureRecognizer *swipeGestureRecognizer;

@property (nonatomic)  NSMutableDictionary *subviewsKeyTagPair;

@property (nonatomic) CGFloat externalViewZoomRatio;

@end

@implementation SlidesViewController

@synthesize deviceView;
@synthesize externalView;
@synthesize externalWindow;
@synthesize backgroundColor = _backgroundColor;
@synthesize subviews = _subviews;
@synthesize swipeGestureRecognizer;

@synthesize externalViewZoomRatio = _externalViewZoomRatio;


@synthesize subviewsKeyTagPair = _subviewsKeyTagPair;

//#define CGRectMakeScale(rect, prevRatio, newRatio)  \
    CGRectMake(rect.origin.x / prevRatio * newRatio, \
               rect.origin.y / prevRatio * newRatio, \
               rect.size.width / prevRatio * newRatio, \
               rect.size.height / prevRatio * newRatio)
//#define CGPointMakeScale(point, prevRatio, newRatio) \
    CGPointMake(point.x / prevRatio * newRatio, \
                point.y / prevRatio * newRatio)

static inline CGRect CGRectMakeScale(CGRect rect, double prevRatio, double newRatio) {
    double tmpScale = newRatio / prevRatio;
    return CGRectMake(rect.origin.x * tmpScale,
                      rect.origin.y * tmpScale,
                      rect.size.width * tmpScale,
                      rect.size.height * tmpScale);
}

static inline CGPoint CGPointMakeScale(CGPoint point, double prevRatio, double newRatio) {
    double tmpScale = newRatio / prevRatio;
    return CGPointMake(point.x * tmpScale,
                       point.y * tmpScale);
}

- (void) setExternalViewZoomRatio:(CGFloat)externalViewZoomRatio {
    
    externalView.frame = CGRectMakeScale(self.externalView.frame, _externalViewZoomRatio, externalViewZoomRatio);
//    externalView.bounds = CGRectMakeScale(self.externalView.bounds, _externalViewZoomRatio, externalViewZoomRatio);
    
    for (UIView *tmp in self.externalView.subviews) {
        tmp.frame = CGRectMakeScale(tmp.frame, _externalViewZoomRatio, externalViewZoomRatio);
//        tmp.bounds = CGRectMakeScale(tmp.bounds, _externalViewZoomRatio, externalViewZoomRatio);
    }
    
    _externalViewZoomRatio = externalViewZoomRatio;
    return; 
}

- (UIView *) view {
    [NSException raise: @"Error" format:nil];
    return nil;
}

- (void) setSubviewsFrame: (CGRect) frame {
    self.deviceView.frame = frame;
    self.externalView.frame = CGRectMakeScale(frame, 1, self.externalViewZoomRatio);
    return; 
}

- (NSMutableDictionary *) subviewsKeyTagPair {
    if (_subviewsKeyTagPair == nil) {
        _subviewsKeyTagPair = [[NSMutableDictionary alloc] init];
    }
    return _subviewsKeyTagPair;
}

- (void) addSubview: (UIView *) inputView {
    UIView *copyOfView;
    
    //NSLog([NSString stringWithFormat: @"Ratio = %g", self.externalViewZoomRatio]);
    
    if ([inputView isMemberOfClass: [UIImageView class]]) {
        UIImageView *srcView = (UIImageView *) inputView;
        UIImageView *destView;
        
        destView = [[UIImageView alloc] initWithImage: srcView.image];
        
        destView.contentMode = srcView.contentMode;
        
        // Other detail of copying UIImageView
        
        copyOfView = (UIView *) destView;
    } else if ([inputView isMemberOfClass:[VPCanvas class]]) {
        VPCanvas *srcView = (VPCanvas *) inputView;
        VPCanvas *destView;
        
        destView = [[VPCanvas alloc] initWithFrame:srcView.frame];
        destView.frame = CGRectMakeScale(srcView.frame, 1, self.externalViewZoomRatio);

        copyOfView = (UIView *) destView;
    } else if ([inputView isMemberOfClass: [UIView class]]) {
        // TODO Copying detail of __ class
    } else if ([inputView conformsToProtocol:@protocol(NSCoding)]) {
        // TODO Protocol available
    } else {
        // Unknown class ... do nothing
        [NSException raise:@"Unsupported class" format:@"%@ is not supported to add in external view", [inputView class]];
        return;
    }

    copyOfView.frame = CGRectMakeScale(inputView.frame, 1, self.externalViewZoomRatio);

    copyOfView.tag = inputView.tag;
    copyOfView.backgroundColor = inputView.backgroundColor;
    
    [deviceView addSubview: inputView];
    [externalView addSubview: copyOfView];
    
    return; 
}

- (void) addSubview: (UIView *) inputView withKey: (NSString *) key {
    [self.subviewsKeyTagPair setObject: [NSNumber numberWithInt:inputView.tag] forKey: key];
    return [self addSubview: inputView];
}

- (void) addSubview:(UIView *)inputView withKey:(NSString *)key withTag: (NSInteger) tag {
    inputView.tag = tag;
    return [self addSubview:inputView withKey:key];
}

- (void) perform: (void (^) (UIView *)) action onTag: (NSInteger) tag {
    action([self.deviceView viewWithTag: tag]);
    action([self.externalView viewWithTag: tag]);
}

- (void) perform: (void (^) (UIView *)) action onKey: (NSString *) key {
    return [self perform: action onTag: [[self.subviewsKeyTagPair objectForKey: key] intValue]];
}

- (void) perform: (void (^) (UIView *, CGPoint)) action onTag: (NSInteger) tag withPoint: (CGPoint) point {
    action([self.deviceView viewWithTag: tag], point);
    action([self.externalView viewWithTag: tag], CGPointMakeScale(point, 1, self.externalViewZoomRatio));
}

- (void) perform: (void (^) (UIView *, CGPoint)) action onKey: (NSString *) key withPoint: (CGPoint) point {
    return [self perform: action onTag: [[self.subviewsKeyTagPair objectForKey: key] intValue] withPoint:point];
}

- (UIView *) enableExternalView:(UIScreen *)externalScreen {
    if (self.externalWindow == nil) {
        self.externalWindow = [[UIWindow alloc] initWithFrame:externalScreen.bounds];
    }
    externalWindow.screen = externalScreen;

    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithNibName:@"ExternalRootViewController" bundle:nil];
    
    UIView *superViewForExternalScreen = [navigationController view]; //[[UINavigationController alloc] initWithFrame:externalScreen.bounds];
  
    externalWindow.rootViewController = superViewForExternalScreen;
//    [externalWindow addSubview:superViewForExternalScreen];
    superViewForExternalScreen.backgroundColor = [UIColor redColor];
        
    self.externalViewZoomRatio = MIN(externalScreen.bounds.size.height / self.deviceView.frame.size.height, externalScreen.bounds.size.width / self.deviceView.frame.size.width);

    [superViewForExternalScreen addSubview:externalView];
    externalWindow.hidden = NO;
    
//    superViewForExternalScreen.zoomScale = 3.0;
    
    superViewForExternalScreen.contentMode = UIViewContentModeScaleAspectFit;
    superViewForExternalScreen.hidden = NO;
    
    NSLog([externalWindow description]);

    return externalView;
}

/* Demo hard-code */
//- (void) setBackgroundColor:(UIColor *)backgroundColor {
//    _backgroundColor = backgroundColor;
//    float r, g, b, alpha;
//    [backgroundColor getRed:&r green:&g blue:&b alpha:&alpha];
//    deviceView.backgroundColor = _backgroundColor;
//    externalView.backgroundColor = _backgroundColor;
//    ((UILabel *) [deviceView viewWithTag:100]).textColor = [[UIColor alloc] initWithRed:(1.-r) green:(1.-g) blue:(1.-b) alpha:1.];
//    ((UILabel *) [externalView viewWithTag:100]).textColor = [[UIColor alloc] initWithRed:(1.-r) green:(1.-g) blue:(1.-b) alpha:1.];
//    return;
//}

//- (void) demoChangeText: (NSString *) str {
//    UILabel *deviceCenterText = (UILabel *) [deviceView viewWithTag: 100];
//    UILabel *externalCenterText = (UILabel *) [externalView viewWithTag: 100];
//    
//    if (deviceCenterText == nil) {
//        deviceCenterText = [[UILabel alloc] initWithFrame: self.deviceView.frame];
//        deviceCenterText.textColor = [UIColor redColor];
//        deviceCenterText.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
//        deviceCenterText.textAlignment = NSTextAlignmentCenter;
//        deviceCenterText.tag = 100;
//        [self.deviceView addSubview: deviceCenterText];
//    }
//    deviceCenterText.text = str;
//    
//    if (externalCenterText == nil) {
//        externalCenterText = [[UILabel alloc] initWithFrame: self.externalView.frame];
//        externalCenterText.textColor = [UIColor redColor];
//        externalCenterText.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
//        externalCenterText.textAlignment = NSTextAlignmentCenter;
//        externalCenterText.tag = 100;
//        [self.externalView addSubview: externalCenterText];
//    }
//    externalCenterText.text = str;
//}

- (UIViewController *) init {
    self = [super init];
    self.externalViewZoomRatio = 1; 
    deviceView = [[UIView alloc] init];
    externalView = [[UIView alloc] init];
    self.backgroundColor = [UIColor greenColor];
    
    self.swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget: deviceView action:@selector(handleSwipeFrom:)];
    deviceView.multipleTouchEnabled = true;
    
    return self;
}

/*
 - (IBAction) handleSwipeFrom: (UISwipeGestureRecognizer *) recognizer {
    CGPoint location = [recognizer locationInView: deviceView];
    NSLog(NSStringFromCGPoint(location));

    switch (recognizer.direction) {
        case UISwipeGestureRecognizerDirectionRight:
            NSLog(@"SwipeGesture: Right");
            break;
        case UISwipeGestureRecognizerDirectionLeft:
            NSLog(@"SwipeGesture: Left");
            break;
        case UISwipeGestureRecognizerDirectionUp:
            NSLog(@"SwipeGesture: Up");
            break;
        case UISwipeGestureRecognizerDirectionDown:
            NSLog(@"SwipeGesture: Down");
            break;
        default:
            NSLog(@"Hihi");
    }

}
 */

- (void) disableExternalView {
    [externalView removeFromSuperview]; 
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
