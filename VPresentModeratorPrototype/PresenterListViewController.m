//
//  SecondViewController.m
//  TabAppTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "PresenterListViewController.h"

@interface PresenterListViewController ()

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@property (strong, nonatomic) NSArray* sectionLabels;

@property (strong, nonatomic) VPModeratorSplitViewController* splitVC;
@property (strong, nonatomic) NSArray* presenterArray;

@end

@implementation PresenterListViewController

//@synthesize presenterList;
@synthesize sectionLabels;
//@synthesize currentPresenter;
@synthesize mainTableView;

@synthesize splitVC;
@synthesize presenterArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self loadContent];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.title = self.tabBarItem.title;

    [self loadContent];
}

-(void)loadContent {
    //VPModeratorPresenter* tempA = [[VPModeratorPresenter alloc]init];
    //[tempA.timer setEachSecondAction:self selector:@selector(updateTimer)];
    //VPModeratorPresenter* tempB = [[VPModeratorPresenter alloc]init];
    //[tempB.timer setEachSecondAction:self selector:@selector(updateTimer)];
    
    // Init. of cell labels
    //presenterList = [[NSArray alloc]initWithObjects:tempA, tempB, nil];
    // Init. of section labels
    if(sectionLabels == nil){
        sectionLabels = [[NSArray alloc] initWithObjects: @"Presenter List", @"", nil];
    }
    // Set nil to currentPresenter
    //self.currentPresenter = nil;
    splitVC = (VPModeratorSplitViewController*)self.splitViewController;
    presenterArray = [self.splitVC.presenterList allValues];
}


- (void)updateTimer{
    //NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:0];
    //[self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    DetailViewController* detailViewController = [self.splitViewController.viewControllers objectAtIndex:1];
    detailViewController.presenterLabel.text = splitVC.currentPresenter.presenterName;
    detailViewController.timerLabel.text = [splitVC.currentPresenter.timer currentTimeInString];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setMainTableView:nil];
    [super viewDidUnload];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return splitVC.presenterList.count;
            break;
            
        case 1:
            return 1;
        break;
    }
    
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([indexPath section]) {
        // Section 0: Actual Presenter List
        case 0:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Presenter Cell"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Presenter Cell"];
            }
            
            // Config the cell...
            VPModeratorPresenter* tempPresenter = [self.presenterArray objectAtIndex:[indexPath row]];
            cell.textLabel.text = tempPresenter.presenterName;
            if(tempPresenter == splitVC.currentPresenter)
                cell.detailTextLabel.text = @"Presenting";
            else
                cell.detailTextLabel.text = [tempPresenter.timer currentTimeInString];
            return cell;
        }
            break;
            
        // Section 1: Cut all Presenter Control
        case 1:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Reset Cell"];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Reset Cell"];
            }
            
            // Config the cell...
            cell.textLabel.text = @"Stop Presenter Control";
            return cell;
        }
            break;
    }
    
    return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [sectionLabels objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Do some stuff when the row is selected
    
    switch ([indexPath section]) {
        // Section 0: Actual Presenter List
        case 0:
        {
            // Get the VPPresenter object
            VPModeratorPresenter* selectedPresenter = [presenterArray objectAtIndex:[indexPath row]];
            
            // if currentPresenter == selectedPresenter, do nothing
            if(!(splitVC.currentPresenter == selectedPresenter)){
                // if currentPresenter != nil, switch turn
                if(splitVC.currentPresenter != nil){
                    // Current Presenter
                    [splitVC.currentPresenter.timer pause];
                
                    // Next Presenter
                    [selectedPresenter.timer start];
                    
                    // Switch Turn
                    splitVC.currentPresenter = selectedPresenter;
                } else {
                    splitVC.currentPresenter = selectedPresenter;
                    [splitVC.currentPresenter.timer start];
                }
            }
        }
            break;
            
        // Section 1: Cut all Presenter Control
        case 1:
        {
            [splitVC.currentPresenter.timer pause];
            splitVC.currentPresenter = nil;
            
            // Update detailViewController
            DetailViewController* detailViewController = [self.splitViewController.viewControllers objectAtIndex:1];
            detailViewController.presenterLabel.text = @"";
            detailViewController.timerLabel.text = @"00:00:00";
        }
            break;
    }
    
    NSIndexSet* tempSection = [[NSIndexSet alloc] initWithIndex:0];
    [self.mainTableView reloadSections:tempSection withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
