//
//  DetailViewController.m
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 18/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPModeratorDetailViewController.h"
#import "VPModeratorSplitViewController.h"

#define DRAW_POINT_FREQ 4

@interface VPModeratorDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@property (strong, nonatomic) VPModeratorSplitViewController *splitVC;

@property (nonatomic) int touchCount;
@end

@implementation VPModeratorDetailViewController

// ViewDemo synthesize
@synthesize mainSlidesContainer;
//@synthesize monitorOutputSwitch;
//@synthesize connectExternalMonitor;
@synthesize slides;
@synthesize imageList;
@synthesize indexFileTextField;
//@synthesize topNavigationItem;
@synthesize timerLabel;
@synthesize debugString;
//@synthesize presentTimer;
// End of ViewDemo synthesize
@synthesize splitVC;
@synthesize touchCount;

// Original ViewDemo Contents
- (void) appendLog: (NSString *) msg {
    //self.logView.text = [logView.text stringByAppendingString:msg];
    if (self.debugString == Nil) {
        self.debugString = [[NSString alloc]initWithString:msg];
    } else {
        self.debugString = [self.debugString stringByAppendingString:msg];
    }
}


- (void) showErrorPrompt: (NSString *) title description: (NSString *) description {
    [[[UIAlertView alloc] initWithTitle:title message:description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void) screenDidChange: (NSNotification *) notification {
    NSArray *screens = [UIScreen screens];
	self.numberOfScreen = [screens count];
    
    //connectExternalMonitor.text = [NSString stringWithFormat: @"%d", self.numberOfScreen - 1];
    
    if (self.numberOfScreen > 1) {
        [slides enableExternalView: [screens objectAtIndex: 1]];
        [self appendLog: @"Trying to add screen\n"];
    } else {
        [slides disableExternalView];
        [self appendLog: @"Have to remove screen\n"];
    }
}

// toggle output and some other thing here
// NOT IN USE: IB switch moved to table view, Outlet deleted
- (IBAction)swtExternalOutput:(UISwitch*)sender {
    if (sender.on) {
        if (self.numberOfScreen > 1) {
            [self.slides enableExternalView: [[UIScreen screens] objectAtIndex: 1]];
        } else {
            [self showErrorPrompt:@"No external monitor" description:@"Cannot enable external monitor. You did not connect to external monitor"];
            sender.on = NO;
        }
    } else {
        [self.slides disableExternalView];
    }
    [self appendLog: [NSString stringWithFormat: @"External Output Mode: %@\n", (sender.on ? @"YES" : @"NO")]];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // TODO
    
    
    /* == Old Code ==
    CGPoint point = [[touches anyObject] locationInView:self.mainSlidesContainer];
    
    self.touchCount = 0;
    
    VPCanvas *canvas = (VPCanvas *)([self.slides.deviceView viewWithTag:124]);
    
    // Immediate drawing
    //    [canvas penDown:point];
    
    VPModeratorPresenter *currentPresenter = self.splitVC.currentPresenter;
    
    if (canvas.update) {
        [currentPresenter sendDrawPath:point color:canvas.color size:canvas.thickness start:YES end:NO];
    } else {
        [currentPresenter sendDrawPath:point start:YES end:NO];
    }
    
    //    [self.slides perform: ^(UIView * inputView, CGPoint point) {
    //        [((VPCanvas *) inputView) penDown:point];
    //    } onKey: @"canvas" withPoint: [[touches anyObject] locationInView: self.mainSlidesContainer]];
     */
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    // TODO
    
    /* == Old Code ==
    CGPoint point = [[touches anyObject] locationInView:self.mainSlidesContainer];
    
    self.touchCount += 1;
    
    if (self.touchCount % DRAW_POINT_FREQ)
        return;
    
    // Immediate drawing
    //    VPCanvas *canvas = (VPCanvas *)([self.slides viewWithTag:124]);
    //    [canvas penMove:point];
    
    [self.splitVC.currentPresenter sendDrawPath:point start:NO end:NO];
    
    //    [self.slides perform: ^(UIView * inputView, CGPoint point) {
    //        [((VPCanvas *) inputView) penMove:point];
    //    } onKey: @"canvas" withPoint: [[touches anyObject] locationInView: self.mainSlidesContainer]];
     */
}

- (IBAction)undoClicked:(id)sender {
}

- (IBAction)redoClicked:(id)sender {
}

// Just for displaying the top natvigation bar
- (NSString *)todayDate
{
    NSLog(@"called");
//    NSDate *today = [NSDate date];
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
//    NSDateComponents *components = [gregorian components:unitFlags fromDate:today];
//    return [NSString stringWithFormat:@"%.4d-%.2d-%.2d",[components year],[components month],[components day]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set change color button invisble
    CGRect frame = {
        {0, 0},
        {self.mainSlidesContainer.frame.size.width, self.mainSlidesContainer.frame.size.height}
    };
    
    NSLog(@"Input frame = %@", NSStringFromCGRect(frame));
    self.slides = [[VPSlidesViewController alloc] init];
    self.slides.backgroundColor = [UIColor grayColor];
    [self addChildViewController: self.slides];
    
    [self.slides setSubviewsFrame:frame];
    [self.mainSlidesContainer addSubview: self.slides.deviceView];

    PSPDFViewController *pdfViewController = [[PSPDFViewController alloc] init];
    pdfViewController.rotationLockEnabled = YES;
    pdfViewController.textSelectionEnabled = NO;
    pdfViewController.linkAction = nil;
    pdfViewController.imageSelectionEnabled = NO;
    [pdfViewController.view setFrame:frame];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:pdfViewController];
    [navController.view setFrame:frame];
    
    [self.slides.deviceView addSubview:navController.view];
    [self.slides addSubViewController:pdfViewController];
    
    [self addChildViewController:navController];

    pdfViewController.leftBarButtonItems = nil;
    self.slides.showingPDF = nil; //[PSPDFDocument PDFDocumentWithURL:[[NSBundle mainBundle] URLForResource:@"chapter2_part1" withExtension:@"pdf"]];

    NSMutableArray *arr = [pdfViewController.rightBarButtonItems mutableCopy];
    [arr addObject:pdfViewController.annotationButtonItem];
    pdfViewController.annotationButtonItem.annotationToolbar.delegate = self.slides; 
    pdfViewController.rightBarButtonItems = arr;

//    NSLog([pdfViewController description]);
//    pdfViewController.documentLabel = @"";
    
//    NSLog([pdfViewController.leftBarButtonItems description]);
    
//    navController.title = @"hi";
    
//    [self.mainSlidesContainer addSubview: pdfViewController.view];


//    [self transitionFromViewController:self.slides toViewController:pdfViewController duration:4 options:UIViewAnimationOptionAllowAnimatedContent animations:nil completion:nil];
    
//    VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
//    [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
    
    [self screenDidChange: Nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidChange:)
												 name:UIScreenDidConnectNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidChange:)
												 name:UIScreenDidDisconnectNotification
											   object:nil];
    
    // Other VC reference
    self.splitVC = (VPModeratorSplitViewController*)self.splitViewController;
    
    [self configureView];
    return;
}

-(void) displayAlert:(NSString *) str {
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:@"Alert"
                               message:str
                              delegate:self
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil];
    [alert show];
    //[alert release];
}

-(void)listFilesFromDocumentsFolder:(NSString*) indexFileName{
    NSMutableString *filesStr = [NSMutableString stringWithString:@"Image in Documents folder: \n"];
    
    //---init a file manager---
    NSFileManager * filemgr = [NSFileManager defaultManager];
    
    //---get the path of the Documents folder---
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullIndexPath = [documentsDirectory stringByAppendingPathComponent:indexFileName];
    
    if ([filemgr fileExistsAtPath: fullIndexPath ] == YES){
        [self appendLog:@"Index File exists\n"];
        NSData * data = [filemgr contentsAtPath: fullIndexPath];
        [self appendLog: [@"Data size = " stringByAppendingFormat:@"%d\n", data.length]];
        NSString* rawStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSArray* fileList = [rawStr componentsSeparatedByString:@"\n"];
        imageList = [[NSMutableArray alloc]initWithCapacity:[fileList count]];
        
        for (NSString *s in fileList){
            if([s rangeOfString:@".png" options:NSCaseInsensitiveSearch+NSBackwardsSearch].location ||
               [s rangeOfString:@".jpg" options:NSCaseInsensitiveSearch+NSBackwardsSearch].location)
            {
                // Load image file name, add into array list
                [imageList addObject:[documentsDirectory stringByAppendingPathComponent:s]];
                
                // For display alert message
                [filesStr appendFormat:@"%@\n", s];
            }
        }
        
        //[file closeFile];
        [self appendLog:@"Index File Read Complete\n"];
        [self displayAlert:filesStr];
    }
    else{
        [self appendLog:@"Index File not found\n"];
    }
}


- (IBAction)btnLoadImage:(UIButton *)sender {
    // clear image array
    [imageList removeAllObjects];
    
    // Reset currentImage
    self.currentImageIndex = 0;
    
    // Load Files in "Documents" Folder with reference to index file
    [self listFilesFromDocumentsFolder: indexFileTextField.text];
    
    // Remove subview (slide and canvas) of previous slide
    [self.slides perform: ^(UIView * inputView) {
        [inputView removeFromSuperview];
    } onTag: 123];
    [self.slides perform: ^(UIView * inputView) {
        [inputView removeFromSuperview];
    } onTag: 124];
    
    // Create subview for first image
    CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
    
    //NSLog(@"%@", [imageList objectAtIndex:0]);
    UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile:[imageList objectAtIndex:0]]];
    imageView.frame = frame;
    imageView.clipsToBounds = YES;
    [self.slides addSubview:imageView withKey: @"Test image" withTag:123];
    
    VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
    [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
}

- (IBAction)btnPrevSlide:(UIButton *)sender {
    if(self.currentImageIndex > 0){
        // Update index
        self.currentImageIndex--;
        
        // Remove subview (slide and canvas) of previous slide
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 123];
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 124];
        
        // Create subview for new image
        CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile:[imageList objectAtIndex:self.currentImageIndex]]];
        imageView.frame = frame;
        imageView.clipsToBounds = YES;
        [self.slides addSubview:imageView withKey: @"Test image" withTag:123];
        
        VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
        
        [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
    } else {
        [self appendLog:@"Start of Slides\n"];
    }
}

- (IBAction)btnNextSlide:(UIButton *)sender {
    if(self.imageList.count - 1 > self.currentImageIndex){
        // Update index
        self.currentImageIndex++;
        
        // Remove subview (slide and canvas) of previous slide
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 123];
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 124];
        
        // Create subview for new image
        CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile:[imageList objectAtIndex:self.currentImageIndex]]];
        imageView.frame = frame;
        imageView.clipsToBounds = YES;
        [self.slides addSubview:imageView withKey: @"Test image" withTag:123];
        
        VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
        
        [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
        
        
    } else {
        [self appendLog:@"End of Slides\n"];
    }
}


// End of ViewDemo Contents

-(void)updateTimer{
    timerLabel.text = [self.splitVC.currentPresenter.timer currentTimeInString];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    id debugLog = segue.destinationViewController;
    
    [debugLog setValue:self.debugString forKey:@"debugString"];
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
    
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Menu", @"Menu");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
    //return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidUnload {
    [self setMainSlidesContainer:nil];
    //[self setConnectExternalMonitor:nil];
    [self setIndexFileTextField:nil];
    //[self setTopNavigationItem:nil];
    //[self setTimerLabel:nil];
    [self setPresenterLabel:nil];
    [super viewDidUnload];
}
@end
