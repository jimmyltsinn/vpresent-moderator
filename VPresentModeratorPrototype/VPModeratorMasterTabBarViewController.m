//
//  RightTabBarViewController.m
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPModeratorMasterTabBarViewController.h"

@interface VPModeratorMasterTabBarViewController ()

@end

@implementation VPModeratorMasterTabBarViewController

@synthesize leftBarNavi;
@synthesize leftBarTabBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Title of the Tab bar items
    NSArray *tabBarItemTitles = [NSArray arrayWithObjects: @"Screen", @"Presenters", @"Requests", @"Control", nil];
    
    for (UIViewController *viewController in self.viewControllers)
    {
        viewController.title = [tabBarItemTitles objectAtIndex: [self.viewControllers indexOfObject: viewController]];
    }
    
    for (UITabBarItem *items in self.tabBar.items)
    {
        items.title = [tabBarItemTitles objectAtIndex: [self.tabBar.items indexOfObject: items]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLeftBarNavi:nil];
    [self setLeftBarTabBar:nil];
    [super viewDidUnload];
}
@end
