//
//  VPPresenter.h
//  VPServerPrototype
//
//  Created by LEUNG Chak Hang on 27/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Timer.h"
//#import "VPModeratorServer.h"
#import "TCPServer.h"
#import <PSPDFKit/PSPDFKit.h>

@protocol VPModeratorPresenterHandler <NSObject>

/* Return presenter ID */
- (BOOL)registerRequest;
- (BOOL)handleUnregister: (char)presenterID;
- (BOOL)handleControlRequest: (char)presenterID;
- (short)handleSlide: (char)presenterID slide: (UIImage *)image;
- (char)handleControlSignal: (char)presenterID signal: (char)signal;
- (BOOL)handlePathDrawing: (char)presenterID point: (CGPoint)point status:(char)status;
- (BOOL)handlePathDrawingSetting: (UIColor *)color size:(short)size;

- (char)handleControlNextSlide:(char)presenterID;
- (char)handleControlPreviousSlide:(char)presenterID;
- (char)handleControlToSlide:(char)presenterID slide:(int)imageIndex;
//- (char)handleControlBlackScreen:(char)presenterID white:(BOOL)white;
- (char)handleControlClearDrawing:(char)presenterID;
//- (char)handleControlPassSecondaryControl:(char)presenterID toPresenter:(char)toPresenterID;
//- (char)handleControlWithdrawSecondaryControl:(char)presenterID countdown:(int)countdown;
- (char)handleControlReturnControl:(char)presenterID;

- (void)handlePDFDocument:(char)presenterID document:(PSPDFDocument *)document;
- (void)handlePDFAnnotation:(char)presenterID annotation:(PSPDFAnnotation *)annotation onPage:(int)page;
- (void)handlePDFViewState:(char)presentID viewState:(PSPDFViewState *)viewState;

- (BOOL)handlePlay: (char)presenterID filename: (NSString* )videoFilename;
- (BOOL)handlePause: (char)presenterID;
- (BOOL)handleStop: (char)presenterID;
- (BOOL)handleGoto: (char)presenterID goto: (double) second;
- (short)handleVideo: (char)presenterID video: (NSData *)videoData;

@end

@interface VPModeratorPresenter : NSObject

@property (readonly) char presenterID;

@property (nonatomic) id<VPModeratorPresenterHandler> delegate;

@property (nonatomic) Timer* timer;
@property (nonatomic) NSString* presenterName;

- (VPModeratorPresenter *)init: (char)inPresenterID fileHandle:(NSFileHandle*)inFileHandle server:(TCPServer *)inServer;

- (void)grantControl;
- (void)withdrawControl;
- (void)handleUnregister;
- (void)receiveHandle: (NSData *)data;
- (void)sendDrawPath:(CGPoint)point start:(BOOL)start end:(BOOL)end;
- (void)sendDrawPath:(CGPoint)point color:(UIColor *)color size:(char)size start:(BOOL)start end:(BOOL)end;

@end
