//
//  Timer.m
//  VPPresenterPrototype
//
//  Created by Jimmy Sinn on 26/11/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "Timer.h"

@interface Timer()

@property (nonatomic) int ticker;

@property (nonatomic) NSTimer *timer;

@property (nonatomic) id target;
@property (nonatomic) SEL selector;

@end

@implementation Timer

@synthesize ticker;
@synthesize timer = _timer;

@synthesize target;
@synthesize selector;

- (void)start {
    self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(runEachSecond:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    [self.target performSelector:self.selector withObject:[NSNumber numberWithInt:self.ticker]];
    return;
}

- (void)pause {
    [self.timer invalidate];
}

- (void)end {
    self.ticker = 0;
    [self.timer invalidate];
}

- (void)runEachSecond:(NSTimer *)theTimer {
    self.ticker = self.ticker + 1;
//    NSLog([NSString stringWithFormat:@"Time = %d", self.ticker]);
    [self.target performSelector:self.selector withObject:[NSNumber numberWithInt:self.ticker]];
}

- (int)currentTime {
    return self.ticker; 
}

- (NSString *)currentTimeInString {
    short hh, mm, ss;
    ss = self.ticker % 60;
    mm = self.ticker / 60 % 60;
    hh = self.ticker / 60 / 60;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d", hh, mm, ss];
}

- (void)setEachSecondAction: (id)inTarget selector:(SEL)inSelector {
    self.target = inTarget;
    self.selector = inSelector;
    return;
}

- (void)resetEachSecondAction {
    self.target = nil;
    self.selector = nil;
    return;
}

@end