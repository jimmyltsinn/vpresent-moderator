//
//  VPModeratorServer.m
//  VPresentModeratorPrototype
//
//  Created by Jimmy Sinn on 1/12/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "VPModeratorServer.h"

@interface VPModeratorServer()

@property VPModeratorSplitViewController *controller; 
@property (nonatomic) NSMutableData *buffer;
@property (nonatomic) unsigned int expectedDataLength;

@end

@implementation VPModeratorServer
@synthesize buffer = _buffer;
@synthesize expectedDataLength;

- (NSMutableData *)buffer {
    if (_buffer == nil)
        _buffer = [[NSMutableData alloc] init];
    return _buffer;
}

//- (VPModeratorServer *)init {
//    self = [super init];
//    [self registerReceiveCallback:self selector:@selector(receiveData:data:)];
//    return self;
//}

- (VPModeratorServer *)initWithController: (VPModeratorSplitViewController *)controller {
    self = [super init];
    [self registerReceiveCallback:self selector:@selector(receiveData:data:)];
    self.controller = controller;
    return self;
}


/* As a partitioner for data */
- (void)receiveData: (NSFileHandle *)fileHandle data:(NSData *)data {
    [self.buffer appendData:data];
    
    while ([self.buffer length] >= self.expectedDataLength) {
        const char *wholeMessage = [self.buffer bytes];

        if ([self.buffer length] < 8)
            return;
        
        if (self.expectedDataLength != 0) {
            char *msg = malloc(sizeof(char) * self.expectedDataLength);
            
            memcpy(msg, wholeMessage, self.expectedDataLength);
            
            NSData *data = [NSData dataWithBytes:msg length:self.expectedDataLength];
            
            [self handleData:fileHandle data:data]; //Wrong handleData ...

            self.buffer = [NSMutableData dataWithBytes:(wholeMessage + self.expectedDataLength) length:([self.buffer length] - self.expectedDataLength)];
            
            free(msg);
        }
        
        if ([self.buffer length] < 8) {
            self.expectedDataLength = 0;
            return;
        }
        
        unsigned int tmp;
        memcpy(&tmp, wholeMessage + self.expectedDataLength + 4, 4);
        self.expectedDataLength = tmp;
    }
    
    // Check checksum

    return;
}

- (void)handleData: (NSFileHandle *)fileHandle data:(NSData *)data {
    const char *msg = [data bytes];
    NSNumber *presenterID = [NSNumber numberWithChar:msg[1]];
    VPModeratorPresenter *presenter = [self.controller.presenterList objectForKey:presenterID];

    if (presenter == nil) {
        char clientID;
        //Checksum
        if (msg[0] != 0x01) return;
        if (msg[1] != 0xFF && msg[1] > 0) return;

        for (clientID = 1; clientID < 255; ++clientID) {
            if ([self.controller.presenterList objectForKey:[NSNumber numberWithChar:clientID]] == nil)
                break;
        }

        presenter = [[VPModeratorPresenter alloc] init:clientID fileHandle:fileHandle server:self];
        presenterID = [NSNumber numberWithChar:clientID];

        presenter.delegate = self.controller;

        [self.controller.presenterList setObject:presenter forKey:presenterID];
    }
    
    [presenter receiveHandle:data];
    
    if ([presenterID charValue] == 0xFF) {
        [self.controller.presenterList removeObjectForKey:presenterID];
        presenterID = nil;
    }

}

@end
