//
//  RightTabBarViewController.h
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftTabBarViewController : UITabBarController
@property (strong, nonatomic) IBOutlet UINavigationItem *leftBarNavi;
@property (strong, nonatomic) IBOutlet UITabBar *leftBarTabBar;

@end
