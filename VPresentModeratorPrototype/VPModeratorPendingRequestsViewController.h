//
//  PendingRequestsViewController.h
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 20/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VPModeratorPendingRequestsViewController : UITableViewController

//@property (strong, nonatomic) NSArray* cellLabels;
//@property (strong, nonatomic) NSArracounty* requestList;

@property NSNumber *notificationCount;

- (void)reloadRequestListView;

@end
