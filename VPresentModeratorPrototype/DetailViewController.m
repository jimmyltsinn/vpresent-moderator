//
//  DetailViewController.m
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 18/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end


@implementation DetailViewController

// ViewDemo synthesize
@synthesize mainSlidesContainer;
//@synthesize monitorOutputSwitch;
//@synthesize connectExternalMonitor;
@synthesize slides;
@synthesize imageList;
@synthesize indexFileTextField;
//@synthesize topNavigationItem;
@synthesize timerLabel;
@synthesize debugString;
//@synthesize presentTimer;
// End of ViewDemo synthesize

// Original ViewDemo Contents
- (void) appendLog: (NSString *) msg {
    //self.logView.text = [logView.text stringByAppendingString:msg];
    if (self.debugString == Nil) {
        self.debugString = [[NSString alloc]initWithString:msg];
    } else {
        self.debugString = [self.debugString stringByAppendingString:msg];
    }
}


- (void) showErrorPrompt: (NSString *) title description: (NSString *) description {
    [[[UIAlertView alloc] initWithTitle:title message:description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void) screenDidChange: (NSNotification *) notification {
    NSArray *screens = [UIScreen screens];
	self.numberOfScreen = [screens count];
    
    //connectExternalMonitor.text = [NSString stringWithFormat: @"%d", self.numberOfScreen - 1];
    
    if (self.numberOfScreen > 1) {
        [slides enableExternalView: [screens objectAtIndex: 1]];
        [self appendLog: @"Trying to add screen\n"];
    } else {
        [slides disableExternalView];
        [self appendLog: @"Have to remove screen\n"];
    }
}

// toggle output and some other thing here
// NOT IN USE: IB switch moved to table view, Outlet deleted
- (IBAction)swtExternalOutput:(UISwitch*)sender {
    if (sender.on) {
        if (self.numberOfScreen > 1) {
            [self.slides enableExternalView: [[UIScreen screens] objectAtIndex: 1]];
        } else {
            [self showErrorPrompt:@"No external monitor" description:@"Cannot enable external monitor. You did not connect to external monitor"];
            sender.on = NO;
        }
    } else {
        [self.slides disableExternalView];
    }
    [self appendLog: [NSString stringWithFormat: @"External Output Mode: %@\n", (sender.on ? @"YES" : @"NO")]];
}

// toggle display mode of content view
// NOT IN USE: IB Segment Control Outlet deleted
- (IBAction)segContentMode:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.slides perform: ^(UIView *inputView) {
                inputView.contentMode = UIViewContentModeScaleToFill;
            } onKey: @"Test image"];
            break;
        case 1:
            [self.slides perform: ^(UIView *inputView) {
                inputView.contentMode = UIViewContentModeScaleAspectFill;
            } onKey: @"Test image"];
            break;
        case 2:
            [self.slides perform: ^(UIView *inputView) {
                inputView.contentMode = UIViewContentModeScaleAspectFit;
            } onKey: @"Test image"];
            break;
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.slides perform: ^(UIView * inputView, CGPoint point) {
        [((VPCanvas *) inputView) penDown:point];
    } onKey: @"canvas" withPoint: [[touches anyObject] locationInView: self.mainSlidesContainer]];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.slides perform: ^(UIView * inputView, CGPoint point) {
        [((VPCanvas *) inputView) penMove:point];
    } onKey: @"canvas" withPoint: [[touches anyObject] locationInView: self.mainSlidesContainer]];
}

- (IBAction)undoClicked:(id)sender {
    [self.slides perform: ^(UIView *inputView) {
        [((VPCanvas *) inputView) undoImage];
    } onKey:@"canvas"];
}

- (IBAction)redoClicked:(id)sender {
    [self.slides perform: ^(UIView *inputView) {
        [((VPCanvas *) inputView) redoImage];
    } onKey:@"canvas"];
}

// Just for displaying the top natvigation bar
- (NSString *)todayDate
{
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:today];
    return [NSString stringWithFormat:@"%.4d-%.2d-%.2d",[components year],[components month],[components day]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set change color button invisble
    CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
    
    self.slides = [[SlidesViewController alloc] init];
    slides.backgroundColor = [UIColor grayColor];
    [self addChildViewController: self.slides];
    
    [slides setSubviewsFrame: frame];
    [mainSlidesContainer addSubview: slides.deviceView];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [self.slides addSubview:imageView withKey: @"Test image" withTag:123];
    
    VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
    [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
    
    [self screenDidChange: Nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidChange:)
												 name:UIScreenDidConnectNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidChange:)
												 name:UIScreenDidDisconnectNotification
											   object:nil];
    
    // Update the date of navigation bar
    //topNavigationItem.title = [self todayDate];
    
    /* Create a timer
    presentTimer = [[Timer alloc]init];
    [presentTimer setEachSecondAction:self selector:@selector(updateTimer:)];
    [presentTimer start];*/
    
    [self configureView];
    return;
}

-(void) displayAlert:(NSString *) str {
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:@"Alert"
                               message:str
                              delegate:self
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil];
    [alert show];
    //[alert release];
}

-(void)listFilesFromDocumentsFolder:(NSString*) indexFileName{
    NSMutableString *filesStr = [NSMutableString stringWithString:@"Image in Documents folder: \n"];
    
    //---init a file manager---
    NSFileManager * filemgr = [NSFileManager defaultManager];
    
    //---get the path of the Documents folder---
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullIndexPath = [documentsDirectory stringByAppendingPathComponent:indexFileName];
    
    if ([filemgr fileExistsAtPath: fullIndexPath ] == YES){
        [self appendLog:@"Index File exists\n"];
        NSData * data = [filemgr contentsAtPath: fullIndexPath];
        [self appendLog: [@"Data size = " stringByAppendingFormat:@"%d\n", data.length]];
        NSString* rawStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSArray* fileList = [rawStr componentsSeparatedByString:@"\n"];
        imageList = [[NSMutableArray alloc]initWithCapacity:[fileList count]];
        
        for (NSString *s in fileList){
            if([s rangeOfString:@".png" options:NSCaseInsensitiveSearch+NSBackwardsSearch].location ||
               [s rangeOfString:@".jpg" options:NSCaseInsensitiveSearch+NSBackwardsSearch].location)
            {
                // Load image file name, add into array list
                [imageList addObject:[documentsDirectory stringByAppendingPathComponent:s]];
                
                // For display alert message
                [filesStr appendFormat:@"%@\n", s];
            }
        }
        
        //[file closeFile];
        [self appendLog:@"Index File Read Complete\n"];
        [self displayAlert:filesStr];
    }
    else{
        [self appendLog:@"Index File not found\n"];
    }
    
    //NSArray *fileList = [filemgr contentsOfDirectoryAtPath:documentsDirectory error:nil];
    //NSMutableString *filesStr = [NSMutableString stringWithString:@"Image in Documents folder: \n"];
    
}


- (IBAction)btnLoadImage:(UIButton *)sender {
    // clear image array
    [imageList removeAllObjects];
    
    // Reset currentImage
    self.currentImageIndex = 0;
    
    // Load Files in "Documents" Folder with reference to index file
    [self listFilesFromDocumentsFolder: indexFileTextField.text];
    
    // Remove subview (slide and canvas) of previous slide
    [self.slides perform: ^(UIView * inputView) {
        [inputView removeFromSuperview];
    } onTag: 123];
    [self.slides perform: ^(UIView * inputView) {
        [inputView removeFromSuperview];
    } onTag: 124];
    
    // Create subview for first image
    CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
    
    //NSLog(@"%@", [imageList objectAtIndex:0]);
    UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile:[imageList objectAtIndex:0]]];
    imageView.frame = frame;
    imageView.clipsToBounds = YES;
    [self.slides addSubview:imageView withKey: @"Test image" withTag:123];
    
    VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
    [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
}

- (IBAction)btnPrevSlide:(UIButton *)sender {
    if(self.currentImageIndex > 0){
        // Update index
        self.currentImageIndex--;
        
        // Remove subview (slide and canvas) of previous slide
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 123];
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 124];
        
        // Create subview for new image
        CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile:[imageList objectAtIndex:self.currentImageIndex]]];
        imageView.frame = frame;
        imageView.clipsToBounds = YES;
        [self.slides addSubview:imageView withKey: @"Test image" withTag:123];
        
        VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
        
        [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
    } else {
        [self appendLog:@"Start of Slides\n"];
    }
}

- (IBAction)btnNextSlide:(UIButton *)sender {
    if(self.imageList.count - 1 > self.currentImageIndex){
        // Update index
        self.currentImageIndex++;
        
        // Remove subview (slide and canvas) of previous slide
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 123];
        [self.slides perform: ^(UIView * inputView) {
            [inputView removeFromSuperview];
        } onTag: 124];
        
        // Create subview for new image
        CGRect frame = {{0, 0}, {mainSlidesContainer.frame.size.width, mainSlidesContainer.frame.size.height}};
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile:[imageList objectAtIndex:self.currentImageIndex]]];
        imageView.frame = frame;
        imageView.clipsToBounds = YES;
        [self.slides addSubview:imageView withKey: @"Test image" withTag:123];
        
        VPCanvas *canvas = [[VPCanvas alloc] initWithFrame:frame];
        
        [self.slides addSubview:canvas withKey:@"canvas" withTag:124];
        
        
    } else {
        [self appendLog:@"End of Slides\n"];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //將page2設定成Storyboard Segue的目標UIViewController
    id debugLog = segue.destinationViewController;
    
    //將值透過Storyboard Segue帶給頁面2的string變數
    [debugLog setValue:self.debugString forKey:@"debugString"];
    
    //NSLog(@"%@", self.debugString);
    
    //將delegate設成自己（指定自己為代理）
    //[page2 setValue:self forKey:@"delegate"];
}

// End of ViewDemo Contents

// Timer part

/*
-(void)updateTimer{
    //static NSInteger count = 0;
    //timerLabel.text = [presentTimer currentTimeInString];
    //NSLog(@"%d", count);
    //count++;
}
*/

// End of Timer part


#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.
}

/*
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}
*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Menu", @"Menu");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
    //return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)viewDidUnload {
    [self setMainSlidesContainer:nil];
    //[self setConnectExternalMonitor:nil];
    [self setIndexFileTextField:nil];
    //[self setTopNavigationItem:nil];
    //[self setTimerLabel:nil];
    [self setPresenterLabel:nil];
    [super viewDidUnload];
}
@end
