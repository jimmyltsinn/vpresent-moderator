//
//  GestureView.m
//  GestureDemo
//
//  Created by Jimmy Sinn on 24/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "Canvas.h"

#define STACK_SIZE 100

@interface Canvas()

@property int penState;
@property CGPoint lastPoint;

@property (nonatomic) NSMutableArray *imageContextStack;
@property (nonatomic) NSMutableArray *imageRedoStack;


@end

@implementation Canvas

@synthesize color = _color;
@synthesize thickness;
@synthesize lastPoint;
@synthesize imageContextStack = _imageContextStack;
@synthesize imageRedoStack = _imageRedoStack;

- (UIColor *)color {
    if (_color == nil)
        _color = [UIColor greenColor];
    return _color; 
}

- (NSMutableArray *) imageRedoStack {
    if (!_imageRedoStack) {
        _imageRedoStack = [[NSMutableArray alloc] initWithCapacity:STACK_SIZE];
    }
    return _imageRedoStack;
    
}
- (NSMutableArray *) imageContextStack {
    if (!_imageContextStack) {
        _imageContextStack = [[NSMutableArray alloc] initWithCapacity:STACK_SIZE];
    }
    return _imageContextStack;
}

- (Canvas *) initWithFrame:(CGRect) frame {
    //NSLog(@"hi");
    self = [super initWithFrame:frame];
    if (self) {
        self.penState = 0;
        self.thickness = 1.0;
    }
    return self;
}

- (Canvas *) initWithImage:(UIImage *)image {
    self = [super initWithImage:image];
    if (self) {
        self.penState = 0;
        self.thickness = 1.0;
    }
    return self;
}

- (void) penDown: (CGPoint) point {
    // Temp disable, waiting to optimize
    //[self pushUIImage: UIImagePNGRepresentation(self.image)];
    self.penState = 1;
    self.lastPoint = point;
    [self penMove: point];
    
    return; 
}

- (void) penMove: (CGPoint) currentPoint {
    [self join: self.lastPoint with: currentPoint]; 
    self.lastPoint = currentPoint;

    return;
}

- (void) penUp: (CGPoint) point {
    self.penState = 0;
    [self penMove: point];
    return;
}

- (void) pushUIImage: (NSData *) obj {
    if (obj == nil)
        obj = [[NSData alloc] initWithData: nil];
    [self.imageContextStack addObject: obj];
    return;
}

- (void) undoImage {
    if ([self.imageContextStack count] == 0) return;
    NSLog(@"Undo Image");
    
    NSData *data = [self.imageContextStack lastObject];
    self.image = [[UIImage alloc] initWithData: data];
    [self.imageContextStack removeLastObject];
    [self.imageRedoStack addObject:data];
    return;
}

- (void) redoImage {
    if ([self.imageRedoStack count] == 0) return;
    
    NSLog(@"Redo image");
    
    NSData *data = [self.imageRedoStack lastObject];
    self.image = [[UIImage alloc] initWithData:data];
    [self.imageRedoStack removeLastObject];
    [self.imageContextStack addObject:data];
    return;
}

- (void) join: (CGPoint) startPoint with: (CGPoint) endPoint {
    CGFloat colors[4];
    
    UIGraphicsBeginImageContext(self.frame.size);
    [self.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), self.thickness);
    
    [self.color getRed:(colors + 0) green:(colors + 1) blue:(colors + 2) alpha:(colors + 3)];
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), colors[0], colors[1], colors[2], colors[3]);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), startPoint.x, startPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), endPoint.x, endPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    self.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return;
}
@end
