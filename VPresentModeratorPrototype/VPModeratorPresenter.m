//
//  VPModeratorPresenter.m
//  VPServerPrototype
//
//  Created by LEUNG Chak Hang on 27/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPModeratorPresenter.h"
#define bit_set(s, pos) s[pos >> 3] |= 1 << (pos & 7)
#define bit_get(s, pos) (s[pos >> 3] & (1 << (pos & 7)))

#define bitc_set(c, pos) c |= 1 << (pos & 7)
#define bitc_reset(c, pos) c &= ~ (1 << (pos & 7))
#define bitc_get(c, pos) (c & (1 << (pos & 7)))

@interface VPModeratorPresenter()

@property (nonatomic) NSFileHandle *fileHandle;
@property (nonatomic) TCPServer *server;

@property char presenterID;

@end

@implementation VPModeratorPresenter

@synthesize delegate;
@synthesize timer;
@synthesize presenterName; 

@synthesize fileHandle;
@synthesize server;

- (VPModeratorPresenter *)init: (char)inPresenterID fileHandle:(NSFileHandle*)inFileHandle server:(TCPServer *)inServer {
    self = [super init];
    self.presenterID = inPresenterID; 
    self.server = inServer; 
    self.fileHandle = inFileHandle;
    self.timer = [[Timer alloc]init];
    return self;
}

- (VPModeratorPresenter *)init {
    [NSException raise:@"Not support" format:@""];
    return nil;
}

- (void)sendData: (NSData *)data {
    [self.server sendDataToClient:self.fileHandle data:data];
    return;
}

- (void)receiveHandle: (NSData *)data {
    const char *raw = [data bytes];

    if (raw[0] != 0x01 && raw[1] != self.presenterID) {
        return;
    }
    
    int len;
    memcpy(&len, raw + 4, 4);
    NSLog([NSString stringWithFormat:@"Receive command ... %d (len = %d)", raw[0], len]);
    switch (raw[0]) {
        case 0x01:
            [self handleRegister:data];
            break;
        case 0x04:
            [self handleUnregister];
            break;
        case 0x06:
            [self handleRequestControl];
            break;
        case 0x0A:
            [self handleSlide:data];
            break;
        case 0x0C:
            [self handleControlSignal:data];
            break;
        case 0x10:
            [self handlePathDrawing:data];
            break;
        case 0x30:
            [self handlePDFData:data];
            break;
        case 0x32:
            [self handlePDFAnnotation:data];
            break;
        case 0x34:
            [self handlePDFViewState:data];
            break;
            
        // video playback part (Jack)
        case 0x20:
            // play
            //NSLog(@"msg play");
            [self handleVideoPlay:data];
            break;
        case 0x22:
            // pause
            [self handleVideoPause];
            break;
        case 0x24:
            // stop
            [self handleVideoStop];
            break;
        case 0x26:
            // goto
            [self handleVideoGoto:data];
            break;
        case 0x28:
            // video receive
            [self handleVideoPrepareSend];
            break;
        case 0x2A:
            // video receive
            [self handleVideo:data];
            break;
        default:
            NSLog(@"@Unknown message ... Command = %d!", raw[0]);
    }
    
    return;
}

- (void)handleRegister: (NSData *)data {
    const char *msg = [data bytes];
    
    if (self.presenterID == 0xFF) {
        [self sendData:[self dataRegisterRespondFailure]];
        return;
    }
    
    unsigned short len;
    
    memcpy(&len, msg + 8, 2);
    
    self.presenterName = [[NSString alloc] initWithBytes:(msg + 10) length:len encoding:NSASCIIStringEncoding];
    
    [self.delegate registerRequest];
    [self sendData:[self dataRegisterRespondSuccess]];
    
    return;
}

/* Jack Start */
- (void)handleVideoPlay: (NSData *)data {
    const char *msg = [data bytes];
    NSData *filenameData;
    
    unsigned int filenameLength;
    memcpy(&filenameLength, msg + 8, 4);
    
    filenameData = [NSData dataWithBytes:(msg + 12) length:filenameLength];
    NSString* filename = [[NSString alloc] initWithData:filenameData encoding:NSUTF8StringEncoding];
    [self.delegate handlePlay: self.presenterID filename:filename];
    [self sendData:[self dataVideoPlayRespond]];
    
    return;
}

- (void)handleVideoPause {
    [self.delegate handlePause: self.presenterID];
    [self sendData:[self dataVideoPauseRespond]];
}

- (void)handleVideoStop {
    [self.delegate handleStop: self.presenterID];
    [self sendData:[self dataVideoStopRespond]];
}

- (void)handleVideoGoto: (NSData *)data {
    const char *msg = [data bytes];
    double d;
    
    // copy start from msg+8 only, first 8-byte is protocol
    memcpy(&d, msg + 8, 8);
    [self.delegate handleGoto: self.presenterID goto:d];
    [self sendData:[self dataVideoGotoRespond]];
}

- (void)handleVideoPrepareSend {
    //    TCPServer* videoServer = [[TCPServer alloc]init];
    //    self.videoServer = [[TCPServer alloc]init];
    //    [self.videoServer listenTo:44400];
    //    [self.videoServer registerReceiveCallback:self selector:@selector(videoServerReceive:data:)];
    
    [self sendData:[self dataVideoPrepareSendRespond]];
}

- (void)handleVideo: (NSData *)data {
    const char *msg = [data bytes];
    NSData *videoData;
    
    unsigned int videoLength;
    memcpy(&videoLength, msg + 8, 4);
    
    videoData = [NSData dataWithBytes:(msg + 12) length:videoLength];
    
    [self.delegate handleVideo:self.presenterID video:videoData];
    [self sendData:[self dataVideoSendRespond]];
    
    return;
}
/* Jack End */

- (void)handleUnregister {
    BOOL ret = [self.delegate handleUnregister:self.presenterID];
    
    if (ret)
        [self sendData: [self dataUnregisterRespond]];
    
    return;
}

- (void)handleRequestControl {
    BOOL ret = [self.delegate handleControlRequest:self.presenterID];
    
    if (ret)
        [self sendData:[self dataControlRequestRespond]];
    return;
}

- (void)grantControl {
    [self sendData:[self dataControlGrant]];
    return;
}

- (void)withdrawControl {
    [self sendData:[self dataControlWithdraw]];
    return; 
}

- (void)handleSlide: (NSData *)data {
    const char *msg = [data bytes];
    NSData *slide;
    CGSize size;
    unsigned short tmp;
    
    memcpy(&tmp, msg + 8, 2);
    size.height = tmp;
    memcpy(&tmp, msg + 10, 2);
    size.width = tmp;
    
    unsigned int imageLength;
    memcpy(&imageLength, msg + 12, 4);
    
    slide = [NSData dataWithBytes:(msg + 16) length:imageLength];
    
    UIImage *slideImage = [[UIImage alloc] initWithData:slide];
    
    short ret = [self.delegate handleSlide:self.presenterID slide:slideImage];
    
    [self sendData:[self dataSlideRespond:ret]];
    
    return;
}

- (void)handleControlSignal: (NSData *)data {
    const char *msg;
    char errorCode = 0x00;
    
    msg = [data bytes];
    
    switch ((unsigned char)msg[8]) {
        case 0x01:
            errorCode = [self.delegate handleControlNextSlide:self.presenterID]; 
            break;
        case 0x02:
            errorCode = [self.delegate handleControlPreviousSlide:self.presenterID];
            break;
        case 0x03:
            errorCode = [self handleControlToSlide:data];
//            errorCode = [self.delegate handleControlToSlide:self.presenterID slide:0];
            break;
//        case 0xF0:
//            errorCode = [self.delegate handleControlBlackScreen:self.presenterID white:NO];
//            break;
        case 0xF1:
            errorCode = [self.delegate handleControlClearDrawing:self.presenterID];
            break;
//        case 0xFD:
//            errorCode = [self.delegate handleControlPassSecondaryControl:self.presenterID toPresenter:0];
//            break;
//        case 0xFE:
//            errorCode = [self.delegate handleControlWithdrawSecondaryControl:self.presenterID countdown:0];
//            break;
        case 0xFF:
            errorCode = [self.delegate handleControlReturnControl:self.presenterID];
            break;
        default:
            NSLog(@"Unknown control signal");
            errorCode = 0xFF;
    }
    
    
    
    errorCode = [self.delegate handleControlSignal:self.presenterID signal:msg[8]];
    
    if (errorCode == 0x00) {
        [self sendData:[self dataControlSignalSuccessRespond]];
    } else {
        [self sendData:[self dataControlSignalFailureRespond:errorCode detail:nil]];
    }
    
    return;
}

- (char)handleControlToSlide:(NSData *)data {
    short page;
    const char *bytes = [data bytes];

    memcpy(&page, bytes + 10, 2);

    return [self.delegate handleControlToSlide:self.presenterID slide:page];
}

- (void)handlePDFData: (NSData *)data {
    unsigned len;
    const char *bytes = [data bytes];
    
    memcpy(&len, bytes + 4, 4);
    len -= 8;
    
    NSData *pdfData = [NSData dataWithBytes:bytes + 8 length:len];
    PSPDFDocument *pdfDocument = [PSPDFDocument PDFDocumentWithData:pdfData];

    [self.delegate handlePDFDocument:self.presenterID document:pdfDocument];
    
    return;
}

- (void)handlePDFAnnotation: (NSData *)data {
    unsigned len;
    const char *bytes = [data bytes];
    int page;
    
    memcpy(&len, bytes + 4, 4);
    memcpy(&page, bytes + 8, 4);
    
    len -= 12;
    
    NSData *annotationData = [NSData dataWithBytes:bytes + 12 length:len];
    
    PSPDFAnnotation *annotation = [NSKeyedUnarchiver unarchiveObjectWithData:annotationData];
    
    [self.delegate handlePDFAnnotation:self.presenterID annotation:annotation onPage:page];
}

- (void)handlePDFViewState: (NSData *)data {
    unsigned len;
    const char *bytes = [data bytes];
    
    memcpy(&len, bytes + 4, 4);
    len -= 8;
    
    NSData *viewStateData = [NSData dataWithBytes:bytes + 8 length:len];
    
    PSPDFViewState *viewState = [NSKeyedUnarchiver unarchiveObjectWithData:viewStateData];
    [self.delegate handlePDFViewState:self.presenterID viewState:viewState];
    
    return;
}

- (NSData *)dataRegisterRespondSuccess {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x02;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);

    //freeWhenDone = NO because msg is not in heap
    return [[NSData alloc] initWithBytes:msg length:length];
}

/* Jack Start */
- (NSData *)dataVideoPlayRespond {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x21;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataVideoPauseRespond {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x23;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataVideoStopRespond {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x25;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataVideoGotoRespond {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x27;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataVideoPrepareSendRespond {
    // TODO
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x29;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataVideoSendRespond {
    // TODO
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x2B;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}
/* Jack End */

- (NSData *)dataRegisterRespondFailure {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x03;
    msg[1] = 0xFF;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);

    //freeWhenDone = NO because msg is not in heap
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataUnregisterRespond {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x05;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataSlideRespond: (unsigned short)slideNumber {
    unsigned int length = 10;
    char msg[10];
    msg[0] = 0x0B;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    memcpy(msg + 8, &slideNumber, 2);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataControlRequestRespond {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x07;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataControlGrant {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x08;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataControlWithdraw {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x09;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataControlSignalSuccessRespond {
    unsigned int length = 8;
    char msg[8];
    msg[0] = 0x0D;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    memcpy(msg + 4, &length, 4);
    return [[NSData alloc] initWithBytes:msg length:length];
}

- (NSData *)dataControlSignalFailureRespond: (char)error detail: (char *)detail {
    unsigned int length = 12;
    char msg[12];
    msg[0] = 0x0E;
    msg[1] = self.presenterID;
    memset(msg + 2, 0 ,2);
    memcpy(msg + 4, &length, 4);
    msg[8] = error;
    if (detail == NULL) {
        memset(msg + 9 ,0, 3);
    } else {
        memcpy(msg + 9, detail, 3);
    }
    
    return [[NSData alloc] initWithBytes:msg length:length];
}

/* Drawing Utilities */
- (void)sendDrawPath: (CGPoint)point start:(BOOL)start end:(BOOL)end{
    NSData *data = [self dataDrawingPath:point start:start end:end ack:NO];
    [self sendData:data];
    return;
}

- (void)sendDrawPath: (CGPoint)point color:(UIColor *)color size:(char)size start:(BOOL)start end:(BOOL)end {
    NSData *data = [self dataDrawingPath:point color:color size:size start:start end:end ack:NO];
    [self sendData:data];
    return;
}

- (NSData *)dataDrawingPath: (CGPoint)point set:(BOOL)set start:(BOOL)start end:(BOOL)end ack:(BOOL)ack{
    unsigned int tmp = 12;
    char msg[12];
    
    msg[0] = 0x10;
    msg[1] = self.presenterID;
    memset(msg + 2, 0, 2);
    
    memcpy(msg + 4, &tmp, 4);
    
    tmp = [self makePacketDrawingContent:point];
    memcpy(msg + 8, &tmp, 4);
    
    msg[8] = [self makePacketDrawingHeader:NO start:start end:end ack:ack];
    
    return [NSData dataWithBytes:msg length:12];
}

- (NSData *)dataDrawingPath: (CGPoint)point start:(BOOL)start end:(BOOL)end ack:(BOOL)ack {
    return [self dataDrawingPath:point set:NO start:start end:end ack:ack];
}

- (NSData *)dataDrawingPath: (CGPoint)point color: (UIColor *)color size: (char)size start:(BOOL)start end:(BOOL)end ack:(BOOL)ack {
    char msg[16];
    
    const char *tmp = [[self dataDrawingPath: point set:YES start:start end:end ack:ack] bytes];
    
    memcpy(msg, tmp, 12);
    
    unsigned int dataLength = 16;
    
    memcpy(msg + 4, &dataLength, 4);
    
    
    int detail = [self makePacketDrawingSetting:color size:size];
    memcpy(msg + 12, &detail, 4);
    
    return [NSData dataWithBytes:msg length:16];
}

- (int)makePacketDrawingContent: (CGPoint)point {
    return htonl((((((unsigned int)point.x) & 0xFFF) << 12) | ((((unsigned int)point.y) & 0xFFF))) & 0xFFFFFF);
}

- (int)makePacketDrawingSetting:(UIColor *)color size: (char)size {
    char msg[4];
    
    const float *colors = CGColorGetComponents(color.CGColor);
    
    msg[0] = (char) (colors[0] * 255.) & 0xFF;
    msg[1] = (char) (colors[1] * 255.) & 0xFF;
    msg[2] = (char) (colors[2] * 255.) & 0xFF;
    msg[3] = (((((char)(colors[3] * 127.)) & 0xF) << 4) | ((char) size & 0xF));
    
    return *(int*)msg;
}

- (char)makePacketDrawingHeader: (BOOL)set start:(BOOL)start end:(BOOL)end ack:(BOOL)ack {
    char msg = 0;
    
    if (set) {
        bitc_set(msg, 0);
    }
    
    if (start) {
        bitc_set(msg, 1);
    }
    
    if (end) {
        bitc_set(msg, 2);
    }
    
    if (ack) {
        bitc_set(msg, 4);
    } else {
        bitc_set(msg, 3);
    }
    
    return msg;
}

- (CGPoint)drawingGetPoint: (const char *)fourByte {
    int tmp = ntohl(*(int *)(fourByte));
    CGPoint point;
    tmp &= 0xFFFFFF;
    point.x = tmp >> 12 & 0xFFF;
    point.y = tmp & 0xFFF;
    return point;
}

- (UIColor *)drawingGetColor: (const char *)nextFourByte {
//    NSLog(@"%d-%d-%d-%d", nextFourByte[0], nextFourByte[1], nextFourByte[2], nextFourByte[3]);
    
    
    
    return [UIColor colorWithRed:((unsigned char)nextFourByte[0] / 255.) green:((unsigned char)nextFourByte[1] / 255.) blue:((unsigned char)nextFourByte[2] / 255.) alpha:((unsigned char)(nextFourByte[3] >> 4) / 127.)];
}

- (char)drawingGetSize: (const char *)nextFourByte {
    return nextFourByte[3] & 0xF;
}

- (BOOL)drawingIsSet: (char)header {
    return (bitc_get(header, 0) ? YES : NO);
}

- (BOOL)drawingIsStart: (char)header {
    return (bitc_get(header, 1) ? YES : NO);
}

- (BOOL)drawingIsEnd: (char)header {
    return (bitc_get(header, 2) ? YES : NO);
}

- (BOOL)drawingIsSyn: (char)header {
    return (bitc_get(header, 3) ? YES : NO);
}

- (BOOL)drawingIsAck: (char)header {
    return (bitc_get(header, 4) ? YES : NO);
}

- (void)handlePathDrawing: (NSData *)data {
    const char *msg = [data bytes];
    CGPoint point;
    UIColor *color;
    char size;
    BOOL ret;
    
    point = [self drawingGetPoint: msg + 8];

    if ([self drawingIsSet:msg[8]]) {
        color = [self drawingGetColor:msg + 12];
        size = [self drawingGetSize:msg + 12];
//        NSLog([color description]);
        ret = [self.delegate handlePathDrawingSetting:color size:size];
        if (ret == NO)
            return;
    }
    
    ret = [self.delegate handlePathDrawing:self.presenterID point:point status:([self drawingIsStart:msg[8]] ? 1 : ([self drawingIsEnd:msg[8]] ? 2 : 0))];

    if (ret == NO)
        return; 

    //Handling echo mechanism
    if ([self drawingIsSyn:msg[8]]) {
        if ([self drawingIsSet:msg[8]]) {
            [self sendData:[self dataDrawingPath:point color:color size:size start:([self drawingIsStart:msg[8]]) end:([self drawingIsEnd:msg[8]]) ack:YES]];
        } else {
            [self sendData:[self dataDrawingPath:point start:([self drawingIsStart:msg[8]]) end:([self drawingIsEnd:msg[8]]) ack:YES]];
        }
    }
    
    return;
}

@end