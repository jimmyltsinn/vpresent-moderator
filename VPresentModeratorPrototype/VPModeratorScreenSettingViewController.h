//
//  FirstViewController.h
//  TabAppTest
//
//  Created by LEUNG Chak Hang on 19/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPModeratorDetailViewController.h"

@interface VPModeratorScreenSettingViewController : UITableViewController

@property (strong, nonatomic) NSArray* cellLabels;
@property (strong, nonatomic) NSArray* sectionLabels;

@end
