//
//  SlidesViewController.m
//  ViewDemo
//
//  Created by Jimmy Sinn on 16/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "VPSlidesViewController.h"

@interface VPSlidesViewController()

@property (strong, nonatomic, readonly) NSMutableSet *subviews;

@property (strong, nonatomic) UIWindow *externalWindow;
@property (nonatomic, strong) IBOutlet UISwipeGestureRecognizer *swipeGestureRecognizer;

@property (nonatomic)  NSMutableDictionary *subviewsKeyTagPair;

@property (nonatomic) CGFloat externalViewZoomRatio;

@property (strong, nonatomic) PSPDFViewController *devicePdfViewController;

@property (strong, nonatomic) MPMoviePlayerController *externalVideoViewController;


@end

@implementation VPSlidesViewController

@synthesize deviceView;
@synthesize externalView;
@synthesize externalWindow;
@synthesize backgroundColor = _backgroundColor;
@synthesize subviews = _subviews;
@synthesize swipeGestureRecognizer;
@synthesize showingPDF = _showingPDF; 

@synthesize externalViewZoomRatio = _externalViewZoomRatio;

@synthesize subviewsKeyTagPair = _subviewsKeyTagPair;

static inline CGRect CGRectMakeScale(CGRect rect, double prevRatio, double newRatio) {
    double tmpScale = newRatio / prevRatio;
    return CGRectMake(rect.origin.x * tmpScale,
                      rect.origin.y * tmpScale,
                      rect.size.width * tmpScale,
                      rect.size.height * tmpScale);
}

static inline CGPoint CGPointMakeScale(CGPoint point, double prevRatio, double newRatio) {
    double tmpScale = newRatio / prevRatio;
    return CGPointMake(point.x * tmpScale,
                       point.y * tmpScale);
}

- (void) setShowingPDF:(PSPDFDocument *)showingPDF {
    NSLog(@"hi");
    NSLog(@"data = %@", [[showingPDF data] description]);
    NSLog(@"dataArray = %@", [[showingPDF dataArray] description]);
    showingPDF.diskCacheStrategy = PSPDFDiskCacheStrategyNothing; 
    self.devicePdfViewController.document = showingPDF;
    //    [self.devicePdfViewController reloadData];
    self.externalPdfViewController.document = [showingPDF copy];
    //    [self.externalPdfViewController reloadData];
    
    showingPDF.delegate = self; 
    _showingPDF = showingPDF;

}

- (void) setExternalViewZoomRatio:(CGFloat)externalViewZoomRatio {
    externalView.frame = CGRectMakeScale(self.externalView.frame, _externalViewZoomRatio, externalViewZoomRatio);
    self.externalPdfViewController.view.frame = CGRectMakeScale(self.externalPdfViewController.view.frame, externalViewZoomRatio, _externalViewZoomRatio);
    self.externalVideoViewController.view.frame = CGRectMakeScale(self.externalVideoViewController.view.frame, externalViewZoomRatio, _externalViewZoomRatio);

    for (UIView *tmp in self.externalView.subviews) {
        tmp.frame = CGRectMakeScale(tmp.frame, _externalViewZoomRatio, externalViewZoomRatio);
    }
    
    _externalViewZoomRatio = externalViewZoomRatio;

    return;
}

- (UIView *) view {
    [NSException raise: @"Error" format:nil];
    return nil;
}

- (void) setSubviewsFrame: (CGRect) frame {
    self.deviceView.frame = frame;
    self.externalView.frame = CGRectMakeScale(frame, 1, self.externalViewZoomRatio);
    return;
}

- (NSMutableDictionary *) subviewsKeyTagPair {
    if (_subviewsKeyTagPair == nil) {
        _subviewsKeyTagPair = [[NSMutableDictionary alloc] init];
    }
    return _subviewsKeyTagPair;
}

@synthesize externalPdfViewController;

- (void) addSubViewController: (UIViewController *)inputViewController {
    UIViewController *copyOfViewController = nil;
    
    if ([inputViewController isMemberOfClass:[PSPDFViewController class]]) {
        self.devicePdfViewController = (PSPDFViewController*)inputViewController;
        PSPDFViewController *srcViewController = (PSPDFViewController *) inputViewController;
        PSPDFViewController *destViewController = [[PSPDFViewController alloc] init];

//        srcViewController.delegate = self; 
        destViewController.document = srcViewController.document;
        
        [destViewController hideControlsAndPageElementsAnimated:YES];
        destViewController.toolbarEnabled = NO;

        self.externalPdfViewController = destViewController;
        copyOfViewController = (UIViewController*) destViewController;
        [destViewController updateSettingsForRotation:UIInterfaceOrientationLandscapeLeft];
    }
    
    copyOfViewController.view.frame = CGRectMakeScale(inputViewController.view.frame, 1, self.externalViewZoomRatio); //self.externalViewZoomRatio);
    [externalView addSubview:copyOfViewController.view];
}

- (void) addVideoViewController: (MPMoviePlayerController *)inputVideoViewController {
    self.externalVideoViewController = inputVideoViewController;
    self.externalVideoViewController.view.frame = CGRectMakeScale(inputVideoViewController.view.frame, 1, self.externalViewZoomRatio);
//    [self.externalPdfViewController.view removeFromSuperview];
    [externalView addSubview:inputVideoViewController.view];
}

- (void)switchToPDF {
    [self.externalVideoViewController.view removeFromSuperview];
    self.externalVideoViewController = nil;
//    [self.externalView addSubview:self.externalPdfViewController.view];
}

- (void)pdfViewController:(PSPDFViewController *)pdfController didShowPageView:(PSPDFPageView *)pageView {
    [self.externalPdfViewController setPage:self.devicePdfViewController.page animated:YES];
}

- (void)pdfViewController:(PSPDFViewController *)pdfController didLoadPageView:(PSPDFPageView *)pageView {
    [self.externalPdfViewController setPage:self.devicePdfViewController.page animated:YES];
}

- (BOOL)pdfViewController:(PSPDFViewController *)pdfController shouldSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
    //Disable selection
    return NO;
}

//- (void)pdfViewController:(PSPDFViewController *)pdfController willShowAnnotationView:(UIView<PSPDFAnnotationViewProtocol> *)annotationView onPageView:(PSPDFPageView *)pageView {
//    NSLog(@"pdfViewController:willShowAnnotationView:onPageView:"); 
//}
//
//- (BOOL)pdfViewController:(PSPDFViewController *)pdfController shouldDisplayAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
//    NSLog(@"pdfViewController:shouldDisplayAnnotation:onPageView: %@", [annotation description]);
//    return NO; 
//}

//- (UIView <PSPDFAnnotationViewProtocol> *)pdfViewController:(PSPDFViewController *)pdfController annotationView:(UIView <PSPDFAnnotationViewProtocol> *)annotationView forAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
//    return annotationView;
//}

//- (void)pdfViewController:(PSPDFViewController *)pdfController didShowAnnotationView:(UIView <PSPDFAnnotationViewProtocol> *)annotationView onPageView:(PSPDFPageView *)pageView {
//    NSLog([annotationView description]); 
//    NSLog(@"pdfViewController:didShowAnnotationView:onPageView:");
//}

- (void)annotationToolbar:(PSPDFAnnotationToolbar *)annotationToolbar didChangeMode:(PSPDFAnnotationToolbarMode)newMode {
    if (newMode == PSPDFAnnotationToolbarNone) {
        [self synchronizeDirtyAnnotation];
        [self.externalPdfViewController reloadData];
    }
}

- (void)synchronizeDirtyAnnotation {
    NSDictionary *dirtyAnnotations = [self.devicePdfViewController.document.annotationParser dirtyAnnotations];
    for (NSNumber *key in dirtyAnnotations) {
        NSMutableArray *array = [dirtyAnnotations objectForKey:key];
        PSPDFAnnotation *annotation = [array lastObject];
        
        NSData *annotationData = [NSKeyedArchiver archivedDataWithRootObject:annotation];
        PSPDFAnnotation *newAnnotation = [NSKeyedUnarchiver unarchiveObjectWithData:annotationData];
        [self.externalPdfViewController.document addAnnotations:[NSArray arrayWithObject:newAnnotation] forPage:[key intValue]];
    }

    [self.devicePdfViewController.document saveChangedAnnotationsWithError:nil];
}

- (void)pdfViewController:(PSPDFViewController *)pdfController didEndPageZooming:(UIScrollView *)scrollView atScale:(CGFloat)scale {
    /* http://stackoverflow.com/questions/868288/getting-the-visible-rect-of-an-uiscrollviews-content */
    PSPDFViewState *viewState = pdfController.viewState;
    
    NSData *viewStateData = [NSKeyedArchiver archivedDataWithRootObject:viewState];
    
    viewState = [NSKeyedUnarchiver unarchiveObjectWithData:viewStateData];
    
    viewState.contentOffset = CGPointMakeScale(viewState.contentOffset, 1, self.externalViewZoomRatio); // viewState.contentOffset;
    [self.externalPdfViewController setViewState:viewState animated:YES];
}

- (void)pdfViewController:(PSPDFViewController *)pdfController didEndPageDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    PSPDFViewState *viewState = pdfController.viewState;
    viewState.contentOffset = CGPointMakeScale(viewState.contentOffset, 1, self.externalViewZoomRatio);
    NSData *viewStateData = [NSKeyedArchiver archivedDataWithRootObject:viewState];
    
    viewState = [NSKeyedUnarchiver unarchiveObjectWithData:viewStateData];
//    viewState.contentOffset = CGPointMakeScale(viewState.contentOffset, 1, self.externalViewZoomRatio); // viewState.contentOffset;

    [self.externalPdfViewController setViewState:viewState animated:YES];
}

- (void) addSubview: (UIView *) inputView {
    UIView *copyOfView;
    
    //NSLog([NSString stringWithFormat: @"Ratio = %g", self.externalViewZoomRatio]);
    
    if ([inputView isMemberOfClass: [UIImageView class]]) {
        UIImageView *srcView = (UIImageView *) inputView;
        UIImageView *destView;
        
        destView = [[UIImageView alloc] initWithImage: srcView.image];
        
        destView.contentMode = srcView.contentMode;
        
        copyOfView = (UIView *) destView;
    } else if ([inputView isMemberOfClass:[VPCanvas class]]) {
        VPCanvas *srcView = (VPCanvas *) inputView;
        VPCanvas *destView;
        
        destView = [[VPCanvas alloc] initWithFrame:srcView.frame];
        destView.frame = CGRectMakeScale(srcView.frame, 1, self.externalViewZoomRatio);
        
        copyOfView = (UIView *) destView;
    } else if ([inputView isMemberOfClass: [UIView class]]) {
        // TODO Copying detail of __ class
    } else if ([inputView conformsToProtocol:@protocol(NSCoding)]) {
        // TODO Protocol available
    } else {
        // Unknown class ... do nothing
        [NSException raise:@"Unsupported class" format:@"%@ is not supported to add in external view", [inputView class]];
        return;
    }
    
    copyOfView.frame = CGRectMakeScale(inputView.frame, 1, self.externalViewZoomRatio);
    
    copyOfView.tag = inputView.tag;
    copyOfView.backgroundColor = inputView.backgroundColor;
    
    [deviceView addSubview: inputView];
    [externalView addSubview: copyOfView];
    
    return;
}

- (void) addSubview: (UIView *) inputView withKey: (NSString *) key {
    [self.subviewsKeyTagPair setObject: [NSNumber numberWithInt:inputView.tag] forKey: key];
    return [self addSubview: inputView];
}

- (void) addSubview:(UIView *)inputView withKey:(NSString *)key withTag: (NSInteger) tag {
    inputView.tag = tag;
    return [self addSubview:inputView withKey:key];
}

- (void) perform: (void (^) (UIView *)) action onTag: (NSInteger) tag {
    action([self.deviceView viewWithTag: tag]);
    action([self.externalView viewWithTag: tag]);
}

- (void) perform: (void (^) (UIView *)) action onKey: (NSString *) key {
    return [self perform: action onTag: [[self.subviewsKeyTagPair objectForKey: key] intValue]];
}

- (void) perform: (void (^) (UIView *, CGPoint)) action onTag: (NSInteger) tag withPoint: (CGPoint) point {
    action([self.deviceView viewWithTag: tag], point);
    action([self.externalView viewWithTag: tag], CGPointMakeScale(point, 1, self.externalViewZoomRatio));
}

- (void) perform: (void (^) (UIView *, CGPoint)) action onKey: (NSString *) key withPoint: (CGPoint) point {
    return [self perform: action onTag: [[self.subviewsKeyTagPair objectForKey: key] intValue] withPoint:point];
}

- (void) performOnPDF: (void (^) (PSPDFViewController *)) action {
    action(self.devicePdfViewController);
    action(self.externalPdfViewController);
}

- (void) performOnPDF: (void (^)(PSPDFViewController *, CGPoint))action withPoint:(CGPoint)point {
    action(self.devicePdfViewController, point);
    action(self.externalPdfViewController, CGPointMakeScale(point, 1, self.externalViewZoomRatio));
}

//- (void) performOnPDF: (void (^)(PSPDFViewController *, CGRect))action withRect:(CGRect)rect {
//    action(self.devicePdfViewController, rect);
//    action(self.externalPdfViewController, CGRectMakeScale(rect, 1, self.externalViewZoomRatio));
//}

- (UIView *) enableExternalView:(UIScreen *)externalScreen {
    if (self.externalWindow == nil) {
        self.externalWindow = [[UIWindow alloc] initWithFrame:externalScreen.bounds];
    }

    self.externalWindow.screen = externalScreen;
    
    UIScrollView *superViewForExternalScreen = [[UIScrollView alloc] initWithFrame:externalScreen.bounds];
    
    [self.externalWindow addSubview:superViewForExternalScreen];
    superViewForExternalScreen.backgroundColor = [UIColor blackColor];

    self.externalViewZoomRatio = MIN(externalScreen.bounds.size.height / self.deviceView.frame.size.height, externalScreen.bounds.size.width / self.deviceView.frame.size.width);
    
    [superViewForExternalScreen addSubview:externalView];
    externalWindow.hidden = NO;
    
    self.externalPdfViewController.rotationLockEnabled = YES;
    superViewForExternalScreen.zoomScale = 3.0;
    
    superViewForExternalScreen.contentMode = UIViewContentModeScaleAspectFit;

//    self.externalWindow.rootViewController = self;
    
    return externalView;
}

- (UIViewController *) init {
    self = [super init];
    self.externalViewZoomRatio = 1.0;
    deviceView = [[UIView alloc] init];
    externalView = [[UIView alloc] init];
    
    self.swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget: self.deviceView action:@selector(handleSwipeFrom:)];
    deviceView.multipleTouchEnabled = true;
    
    return self;
}

- (void) disableExternalView {
    [externalView removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
