//
//  VPModeratorServer.h
//  VPresentModeratorPrototype
//
//  Created by Jimmy Sinn on 1/12/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import "TCPServer.h"
#import "VPModeratorSplitViewController.h"

@interface VPModeratorServer : TCPServer
- (VPModeratorServer *)initWithController:(VPModeratorSplitViewController *)controller;
@end
