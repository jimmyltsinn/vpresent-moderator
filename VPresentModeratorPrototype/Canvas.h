//
//  GestureView.h
//  GestureDemo
//
//  Created by Jimmy Sinn on 24/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Canvas : UIImageView

@property float thickness;
@property (nonatomic) UIColor *color;

- (Canvas *) initWithFrame:(CGRect) frame;

- (void) penMove: (CGPoint) currentPoint;
- (void) penUp: (CGPoint) point;
- (void) penDown: (CGPoint) point;
- (void) undoImage;
- (void) redoImage;
@end
