//
//  NetworkUtilities.h
//  ConnectionDemo
//
//  Created by Jimmy Sinn on 22/10/12.
//  Copyright (c) 2012 Jimmy Sinn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <arpa/inet.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>

@interface NetworkUtilities : NSObject

+ (NSString *) getAddressString: (struct sockaddr_in) addr; 
+ (struct sockaddr_in) getStructSockaddr: (NSString *) str;
+ (struct sockaddr_in) getStructSockaddr: (NSString *) ip withPort: (short) port inHostByteOrdering: (BOOL) hostByteOrdering;
+ (NSString *)deviceIPAddress;

@end
