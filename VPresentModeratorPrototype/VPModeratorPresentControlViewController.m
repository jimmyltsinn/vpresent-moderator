//
//  PresentControlViewController.m
//  SplitViewTest
//
//  Created by LEUNG Chak Hang on 20/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import "VPModeratorPresentControlViewController.h"
#import "NetworkUtilities.h"

@interface VPModeratorPresentControlViewController ()

@end

@implementation VPModeratorPresentControlViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.title = self.tabBarItem.title;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Init. of cell lables
    //cellLabels = [[NSArray alloc]initWithObjects:@"640 X 480", @"800 X 600", @"1024 X 768", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch( [indexPath section] ) {
        case 0: {
            UITableViewCell* aCell = [tableView dequeueReusableCellWithIdentifier:@"IP Address"];
            if( aCell == nil ) {
                aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IP Address"];
            }
            aCell.textLabel.text = @"IP Address";
            aCell.selectionStyle = UITableViewCellSelectionStyleNone;
            aCell.userInteractionEnabled = NO;
            aCell.detailTextLabel.text = [NetworkUtilities deviceIPAddress];// @"hihi";//(isConnectingExternal ? @"Yes" : @"No");
            
            return aCell;
        }break;
    }
    return nil;
}

@end
