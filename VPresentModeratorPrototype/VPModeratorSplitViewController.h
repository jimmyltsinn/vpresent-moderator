//
//  VPModeratorSplitViewController.h
//  VPServerPrototype
//
//  Created by Jimmy Sinn on 28/11/12.
//  Copyright (c) 2012 LEUNG Chak Hang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VPModeratorPresenter.h"

@interface VPModeratorSplitViewController : UISplitViewController <VPModeratorPresenterHandler>

@property (nonatomic, strong) NSMutableDictionary *presenterList;
@property (nonatomic, strong) VPModeratorPresenter *currentPresenter;
@property (strong, nonatomic) NSMutableArray* requestList;

@property (strong, nonatomic) UIImage* currentImage;

@end
